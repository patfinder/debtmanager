﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandServer
{
    public class RemoteCommand
    {
        public RemoteCommandType CommandType { get; set; }
    }

    public class GenerateReportCommand : RemoteCommand
    {
        public ReportType ReportType { get; set; }

        public GenerateReportCommand(ReportType reportType)
        {
            CommandType = RemoteCommandType.GenerateReport;
            ReportType = reportType;
        }
    }

    public enum RemoteCommandType
    {
        Unknown,
        GenerateReport,
    }

    public enum ReportType
    {
        Unknown,
        BadDebtReport,
        D04hDocument,
        DebtRemindReport,
        CompanyDebtDetailReport,
        OldDebtProcessingReport,
    }
}
