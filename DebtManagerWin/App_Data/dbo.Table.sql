﻿CREATE TABLE [dbo].[Table]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [CompanyId] NVARCHAR(10) NULL, 
    [PaperDate] DATETIME2 NULL, 
    [PaperNumber] NCHAR(10) NULL, 
    [PaperType] INT NULL, 
    [PaperTypeName] NCHAR(10) NULL, 
    [IssuingOffice] NCHAR(10) NULL, 
    [IssuingOfficeName] NCHAR(10) NULL
)
