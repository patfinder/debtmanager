﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin
{
    public partial class DanhMuc : Form
    {
        public string CurrentItemLabel {
            get
            {
                return lblCurrentItem.Text;
            }
            set
            {
                lblCurrentItem.Text = value;
            }
        }

        public string ItemListLabel
        {
            get
            {
                return lblItemList.Text;
            }
            set
            {
                lblItemList.Text = value;
            }
        }

        public DanhMuc()
        {
            InitializeComponent();
        }
    }
}
