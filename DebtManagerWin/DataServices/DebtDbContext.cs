﻿using DebtManagerWin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.DataServices
{
    public class DebtDbContext : DbContext
    {
        public DebtDbContext() : base("DebtDbContext")
        {
        }

        public DbSet<BadDebt> BadDebts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
