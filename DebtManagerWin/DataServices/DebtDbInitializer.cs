﻿using DebtManagerWin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.DataServices
{
    public class DebtDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<DebtDbContext>
    {
        protected override void Seed(DebtDbContext context)
        {
            var badDebts = new List<BadDebt>
            {
                new BadDebt{CompanyId = "CompanyId", PaperDate = DateTime.Parse("2018-01-01")},
            };

            badDebts.ForEach(s => context.BadDebts.Add(s));
            context.SaveChanges();
        }
    }
}
