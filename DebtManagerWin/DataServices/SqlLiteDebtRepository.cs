﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperExtensions;
using DebtManagerWin.Models;
using SimpleStack.Orm;
using SimpleStack.Orm.Sqlite;

namespace DebtManagerWin
{
    class SqlLiteDebtRepository : SqLiteBaseRepository, IDebtRepository
    {
        private static OrmConnectionFactory Factory = new OrmConnectionFactory(new SqliteDialectProvider(), ConnectionString);

        private static string ConnectionString => $"Data Source={Environment.CurrentDirectory + "\\CongNo.sqlite"}";

        private static void CreateDatabase()
        {
            using (var conn = Factory.OpenConnection())
            {
                conn.CreateTable<Debt>(false);
            }
        }

        private static void CreateDatabase1()
        {
            using (var cnn = SimpleDbConnection())
            {
                cnn.Open();
                cnn.Execute(
                    @"create table Debt
                      (
                         Id            INTEGER(8) identity primary key AUTOINCREMENT,
                         madvi         varchar(100),
                         tendvi        varchar(100),
                         diachi        varchar(100),
                         dsnv          varchar(100),
                         _tien_dk      REAL,
                         sld           REAL,
                         sothang       REAL,
                         _tien_unc     REAL,
                         _laiqh        REAL,
                         _ck_lai       REAL,
                         _tien_ck      REAL,
                         _lai1_ps      REAL,
                         tongBhCk      REAL,
                         tongBst       REAL,
                         tongBsg       REAL
                      )");
            }
        }

        public Debt Get(long id)
        {
            if (!File.Exists(DbFile))
            {
                CreateDatabase();
                return null;
            }

            using (var conn = Factory.OpenConnection())
            {
                return conn.FirstOrDefault<Debt>(x => x.Id == id);
            }
        }
        
        public long Save(Debt debt)
        {
            if (!File.Exists(DbFile))
            {
                CreateDatabase();
            }

            using (var conn = Factory.OpenConnection())
            {
                conn.Insert(debt);
                return conn.QueryFirst("SELECT LAST_INSERT_ROWID()");
            }
        }
        
        public void Save(Debt[] debts)
        {
            if (!File.Exists(DbFile))
            {
                CreateDatabase();
            }

            using (var conn = Factory.OpenConnection())
            {
                conn.Insert<Debt>(debts);
            }
        }
    }
}
