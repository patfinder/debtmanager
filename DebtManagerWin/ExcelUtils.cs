﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using log4net;
using Microsoft.Office.Interop.Excel;
using Application = Microsoft.Office.Interop.Excel.Application;
using Excel = Microsoft.Office.Interop.Excel;

namespace DebtManagerWin
{
    public static class ExcelUtils
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));

        /// <summary>
        /// Read the whole Excel file content.
        /// </summary>
        /// <param name="excelFile"></param>
        /// <param name="columnIndices">Index of columns to read. Start with 1</param>
        /// <returns></returns>
        public static object[][] ReadExcelFile(string excelFile, int[] columnIndices = null)
        {
            //Create COM Objects. Create a COM object for everything that is referenced
            Application xlApp = new Application();
            Workbook xlWorkbook = xlApp.Workbooks.Open(excelFile);
            _Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Range xlUsedRange = xlWorksheet.UsedRange;
            Range xlLastCell = xlWorksheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);

            // Create column list if needed
            if (columnIndices == null)
                columnIndices = Enumerable.Range(1, xlLastCell.Column).ToArray();

            int rowCount = xlUsedRange.Rows.Count;
            int colCount = columnIndices.Length;

            object[][] tableValues = new object[rowCount][];
            
            //iterate over the rows and columns and print to the console as it appears in the file
            //excel is not zero based!!
            for (int row = 1; row <= rowCount; row++)
            {
                tableValues[row - 1] = new object[colCount];

                for (int c = 0; c < columnIndices.Length; c++)
                {
                    var col = columnIndices[c];
                    var cellValue = xlUsedRange.Cells[row, col].Value2;
                    tableValues[row - 1][c] = cellValue;
                }
            }

            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //rule of thumb for releasing com objects:
            //  never use two dots, all COM objects must be referenced and released individually
            //  ex: [somthing].[something].[something] is bad

            //release com objects to fully kill excel process from running in the background
            Marshal.ReleaseComObject(xlLastCell);
            Marshal.ReleaseComObject(xlUsedRange);
            Marshal.ReleaseComObject(xlWorksheet);

            //close and release
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return tableValues;
        }
    }
}
