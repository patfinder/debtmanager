﻿using DebtManagerWin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin
{
    public interface IDebtRepository
    {
        Debt Get(long id);

        long Save(Debt customer);

        void Save(Debt[] customers);
    }
}
