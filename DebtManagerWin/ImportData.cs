﻿using DebtManagerWin.LangUtils;
using DebtManagerWin.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin
{
    public partial class Form1 : Form
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));

        private SqlLiteDebtRepository DebtRepo = new SqlLiteDebtRepository();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnImport_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtImportFile.Text))
                {
                    MessageBox.Show("Please choose import file (click 'Browse')");
                    return;
                }

                var xlsColumns = new[] { 1, 3, 6, 10, 18, 35, 44, 52, 61, 69, 70, 95, 121, 125, 130 };
                var values = ExcelUtils.ReadExcelFile(txtImportFile.Text, xlsColumns);

                if (values.Length <= 1)
                {
                    MessageBox.Show("Xls file is empty.");
                    return;
                }

                // Field names row
                var fieldNames = values[0].ToList().Select(f => f.ToString().ToLower().Trim()).ToList();

                // Get Xls field name
                var xlsFields = AttributeUtils.GetAttributeValues<Debt, XlsField>();

                var debts = new List<Debt>(100);

                // Loop on rows
                for (var i = 1; i < values.Length; i++)
                {
                    // Assign props value
                    var debt = new Debt();
                    for (var j = 0; j < fieldNames.Count(); j++)
                    {
                        var field = fieldNames[j];

                        var prop = xlsFields.First(p => p.Value.Name.ToLower() == field);
                        try
                        {
                            AssignmentUtils.AssignValue(debt, prop.Key, values[i][j]);
                        }
                        catch(Exception ex)
                        {
                            _logger.Error($"Field error: {prop.Key} - Value: {values[i][j]} - Error: {ex.Message}");
                        }
                    }

                    // Save to list
                    debts.Add(debt);

                    // Save to DB
                    if (debts.Count >= 100)
                    {
                        DebtRepo.Save(debts.ToArray());
                        debts.Clear();
                    }
                }

                // Save to DB
                if (debts.Count > 0)
                {
                    DebtRepo.Save(debts.ToArray());
                    debts.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                _logger.Error(ex);
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog()
            {
                Filter = "Excel (*.xls)|*.xls|Excel 2003 (*.xlsx)|*.xlsx"
            };

            if (fileDialog.ShowDialog() != DialogResult.OK)
                return;

            txtImportFile.Text = fileDialog.FileName;
        }
    }
}
