﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.LangUtils
{
    public static class AttributeUtils
    {
        /// <summary>
        /// Get attribute value of all property for specified attribute.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="attrType"></param>
        /// <returns>Dict of prop and its attribute value</returns>
        public static Dictionary<string, AT> GetAttributeValues<T, AT>()
        {
            var attrValues = new Dictionary<string, AT>();
            var objType = typeof(T);

            var props = objType.GetProperties();
            foreach (var prop in props)
            {
                var attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    if (attr is AT)
                    {
                        attrValues.Add(prop.Name, (AT)attr);
                    }
                }
            }

            return attrValues;
        }
    }
    public static class AssignmentUtils
    {
        public static void AssignValue(object obj, string propName, object value)
        {
            var props = obj.GetType().GetProperties();
            foreach (var prop in props)
            {
                if(prop.Name == propName)
                {
                    prop.SetValue(obj, value);
                    break;
                }
            }
        }
    }

}
