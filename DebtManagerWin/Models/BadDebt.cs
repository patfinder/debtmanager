﻿using SimpleStack.Orm.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    /// <summary>
    /// Nợ khó thu
    /// </summary>
    public class BadDebt
    {
        [PrimaryKey]
        public long? Id { get; set; }

        public string CompanyId { get; set; }

        /// <summary>
        /// NGÀY CHỨNG TỪ
        /// </summary>
        public DateTime PaperDate { get; set; }

        /// <summary>
        /// SỐ CHỨNG TỪ
        /// </summary>
        public string PaperNumber { get; set; }

        /// <summary>
        /// LOẠI CHỨNG TỪ
        /// </summary>
        public int PaperType { get; set; }

        [Ignore]
        public string PaperTypeName { get; set; }

        /// <summary>
        /// CƠ QUAN BAN HÀNH CHỨNG TỪ
        /// </summary>
        public int IssuingOffice { get; set; }

        [Ignore]
        public int IssuingOfficeName { get; set; }

        /// <summary>
        /// THÁNG XÁC ĐỊNH ĐƠN VỊ NỢ KHÓ THU
        /// </summary>
        public DateTime DebtMonth { get; set; }

        /// <summary>
        /// LOẠI ĐƠN VỊ
        /// </summary>
        public int BadDebtType { get; set; }

        [Ignore]
        public string BadDebtTypeName { get; set; }
    }
}
