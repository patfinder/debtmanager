﻿using SimpleStack.Orm.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    public class Debt
    {
        [PrimaryKey]
        [XlsField("Id")]
        public long? Id { get; set; }
        [XlsField("madvi")]
        public string madvi { get; set; }
        [XlsField("tendvi")]
        public string tendvi { get; set; }
        [XlsField("diachi")]
        public string diachi { get; set; }
        // value: dung_sms, 
        [XlsField("dsnv")]
        public string dsnv { get; set; }
        [XlsField("_tien_dk")]
        public double _tien_dk { get; set; }
        [XlsField("sld")]
        public double sld { get; set; }
        [XlsField("sothang")]
        public double sothang { get; set; }
        [XlsField("_tien_unc")]
        public double _tien_unc { get; set; }
        [XlsField("_laiqh")]
        public double _laiqh { get; set; }
        [XlsField("_ck_lai")]
        public double _ck_lai { get; set; }
        [XlsField("_tien_ck")]
        public double _tien_ck { get; set; }
        [XlsField("_lai1_ps")]
        public double _lai1_ps { get; set; }
        [XlsField("tongBhCk")]
        public double tongBhCk { get; set; }
        [XlsField("tongBst")]
        public double tongBst { get; set; }
        [XlsField("tongBsg")]
        public double tongBsg { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class XlsField : Attribute
    {
        public string Name { get; set; }

        public XlsField(string name)
        {
            this.Name = name;
        }
    }

}
