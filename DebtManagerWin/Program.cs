﻿using AutoMapper;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin
{
    static class Program
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            XmlConfigurator.Configure();

            _logger.Info("============================================");
            _logger.Info("Application started.");

            //Mapper.Initialize(cfg => {
            //    cfg.CreateMap<Foo, FooDto>();
            //    cfg.CreateMap<Bar, BarDto>();
            //});

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            _logger.Info("Application finished.");
        }
    }
}
