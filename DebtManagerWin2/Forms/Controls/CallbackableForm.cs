﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin.Utils
{
    //public class CallbackableForm : Form
    public class CallbackableForm<T> : Form where T : class
    {
        //public delegate void OnFormCloseHandler(DbSet dbSet);
        public delegate void OnFormCloseHandler(DbSet<T> dbSet);

        protected OnFormCloseHandler _onFormCloseOk;
        public event OnFormCloseHandler OnFormCloseOk
        {
            add
            {
                this._onFormCloseOk += value;
            }
            remove
            {
                this._onFormCloseOk -= value;
            }
        }
    }
}
