﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin.Forms
{
    public partial class CopyableMsgBox : Form
    {
        public string Caption { get => lblCaption.Text; set => lblCaption.Text = value; }

        public string Message { get => txtMessage.Text; set => txtMessage.Text = value; }

        public CopyableMsgBox()
        {
            InitializeComponent();
        }

        public static DialogResult Show(string message, string caption = "")
        {
            return new CopyableMsgBox
            {
                Caption = caption,
                Message = message
            }.ShowDialog();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
