﻿using DebtManagerWin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin.Forms.Controls
{
    public class PageableListView : ListView
    {
        /// <summary>
        /// Start from 0
        /// </summary>
        public int PageIndex { get; set; }

        public int PageCount { get; set; }

        public int PageSize => AppConstants.RowsToDisplay;

        public bool CanMoveLeft => PageIndex > 0;

        public bool MoveLeft()
        {
            if (CanMoveLeft)
            {
                PageIndex--;
                return true;
            }
            return false;
        }

        public bool CanMoveRight => PageIndex < PageCount - 1;

        public bool MoveRight()
        {
            if (CanMoveRight)
            {
                PageIndex++;
                return true;
            }
            return false;
        }

        public int SkipCount => PageIndex * PageSize;
    }
}
