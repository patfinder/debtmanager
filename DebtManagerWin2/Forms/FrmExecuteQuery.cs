﻿using DebtManagerWin.DataServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin.Forms
{
    public partial class FrmExecuteQuery : Form
    {
        private DebtDbContext db => Program.db;

        public FrmExecuteQuery()
        {
            InitializeComponent();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            var result = db.Database.ExecuteSqlCommand(txtQuery.Text);
            Debug.WriteLine($"btnExecute_Click - result: {result}");
        }
    }
}
