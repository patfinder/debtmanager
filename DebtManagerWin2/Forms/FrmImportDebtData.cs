﻿using DebtManagerWin.DataServices;
using DebtManagerWin.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartDev.Libs.Excel;
using SmartDev.Libs;
using System.Configuration;
using DebtManagerWin.Utils;
using DebtManagerWin.Services;

namespace DebtManagerWin
{
    /// <summary>
    /// Nhập dữ liệu nợ định kì
    /// </summary>
    public partial class FrmImportDebtData : Form
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));
        private DebtDbContext db => Program.db;

        private AppConfigReader configReader;

        private int ErrorCount;
        private DateTime importMonth;

        public FrmImportDebtData()
        {
            configReader = new AppConfigReader();

            InitializeComponent();

            cboImportMonth.Items.AddRange(Utils.TimeUtils.GenerateMonthsList(AppConstants.MonthsToShow, DateTime.Now.AddMonths(+1)).ToArray());
            cboImportMonth.ValueMember = nameof(DateListItem.Value);
            cboImportMonth.DisplayMember = nameof(DateListItem.Display);
            cboImportMonth.SelectedIndex = 0;
        }

        private void btnImport_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtImportFile.Text))
            {
                MessageBox.Show("Please choose import file (click 'Browse')");
                return;
            }

            importMonth = new DateTime(((DateListItem)cboImportMonth.SelectedItem).Value.Year, ((DateListItem)cboImportMonth.SelectedItem).Value.Month, 1);

            // Check if data for selected month exists.
            if(db.Debts.Any(d => d.ImportMonth == importMonth))
            {
                if (DialogResult.Yes != MessageBox.Show("Bạn đã import tháng này, bạn có muốn import lại không?\r\n" +
                    "Lưu ý: Bạn có thể xoá dữ liệu đã import trong menu Tác vụ - Xem dữ liệu nợ", "Cảnh báo",
                    MessageBoxButtons.YesNoCancel))
                    return;
            }

            // Check if import month exists
            //if (configReader.GetImportedMonths().Contains(importMonth))
            //{
            //    if (DialogResult.Yes != MessageBox.Show("Bạn đã import tháng này, bạn có muốn import lại không?", "Cảnh báo", 
            //        MessageBoxButtons.YesNoCancel))
            //        return;
            //}

            // Set cursor
            Cursor = Cursors.WaitCursor;

            btnImport.Enabled = false;

            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            backgroundWorker1.RunWorkerAsync();
        }

        private void DoImport()
        {
            int errorCount = 0;
            int savedCount = 0;

            try
            {
                int MaxDebtListLength = 30, iTmp;
                var excelLoad = 30;

                // 10% on first load
                ReportProgress(10, 0);

                var columnSettings = ConfigurationManager.AppSettings["DebtImportColumns"];
                var xlsColumns = columnSettings.Split(',').Select(s => int.Parse(s.Trim())).ToArray();
                var values = ExcelUtils.ReadExcelFile(txtImportFile.Text, xlsColumns);

                if (values.Length <= 1)
                {
                    Invoke(new Action<bool, string>(OnImportError), new object[] { false, "Excel file is empty." });
                    return;
                }

                try
                {
                    ReportProgress(excelLoad, 0);

                    // Field names row
                    var fieldNames = values[0].ToList().Select(f => f.ToString().ToLower().Trim()).ToList();

                    // Get <Field - Xls field name> mapping
                    var xlsFields = AttributeUtils.GetAttributeValues<Debt, XlsFieldName>();

                    _logger.Info($"{nameof(FrmImportDebtData)}-{nameof(DoImport)} - Import Excel field: {string.Join(", ", fieldNames)}");

                    _logger.Info($"{nameof(FrmImportDebtData)}-{nameof(DoImport)} - Import Model fields: {string.Join(", ", xlsFields.Select(f => $"{f.Key}={f.Value.Name}"))}");

                    var debtList = new List<Debt>(MaxDebtListLength);

                    // Loop on rows
                    for (var i = 1; i < values.Length; i++)
                    {
                        // Create new debt
                        var debt = new Debt()
                        {
                            ImportMonth = importMonth,
                        };
                        for (var j = 0; j < fieldNames.Count(); j++)
                        {
                            var field = fieldNames[j];
                            // Find matching prop by Xls field name attr
                            var prop = xlsFields.FirstOrDefault(p => p.Value.Name.ToLower() == field);

                            try
                            {
                                // TODO: improve perf by caching Type info.
                                AssignmentUtils.AssignValue(debt, prop.Key, values[i][j]);
                            }
                            catch (Exception ex)
                            {
                                errorCount++;
                                _logger.Error($"Field error: {prop.Key} - Value: {values[i][j]} - Error: {ex.Message}");
                            }
                        }

                        // Save to list
                        debtList.Add(debt);

                        // Save to DB
                        if (debtList.Count >= MaxDebtListLength)
                        {
                            iTmp = debtList.Count;
                            SaveDebtToDb(debtList);
                            savedCount += iTmp;
                        }

                        // Report each x rows
                        if (i % 30 == 0)
                        {
                            ReportProgress(excelLoad, i / values.Length);
                        }
                    }

                    // Save to DB
                    if (debtList.Count > 0)
                    {
                        iTmp = debtList.Count;
                        SaveDebtToDb(debtList);
                        savedCount += iTmp;
                    }
                }
                catch (Exception ex)
                {
                    Invoke(new Action<bool, string>(OnImportError), new object[] { true, "Exception: " + ex.Message });
                    _logger.Error(ex);
                }

                _logger.Info($"{nameof(FrmImportDebtData)}-{nameof(DoImport)} - Import completd: Read: {values.Length - 1}, Saved: {savedCount}, Error: {errorCount}");
            }
            catch (Exception ex)
            {
                Invoke(new Action<bool, string>(OnImportError), new object[] { false, "Exception: " + ex.Message });
                _logger.Error(ex);
            }

            ErrorCount = errorCount;
        }

        private void SaveDebtToDb(List<Debt> debtList)
        {
            // Extract companies
            ExtractAndUpdateCompanies(debtList);

            // Get existing company list (new company don't have reminds)
            var codeList = debtList.Select(d => d.CompanyCode);
            var comapnies = db.Companies.Where(c => codeList.Contains(c.Code)).ToList();

            // Override old reminds if this month debt is less than 2 months.
            debtList.ForEach(d =>
            {
                var company = comapnies.FirstOrDefault(c => c.Code == d.CompanyCode);
                if (company == null)
                    return;

                // update overriden flag
                company.DebtReminds?.Where(r => !r.IsOverridden).ToList().ForEach(r => r.IsOverridden = true);
            });

            db.Debts.AddOrUpdate(d => new {d.ImportMonth, d.CompanyCode }, debtList.ToArray());
            db.SaveChanges();
            debtList.Clear();
        }

        private void ExtractAndUpdateCompanies(List<Debt> debts)
        {
            var companies = db.Companies.ToList();

            debts.ForEach(debt =>
            {
                Officer officer = null;

                var staffId = (debt.Officers ?? "").Split(',').First().Trim().ToLower();
                if (!string.IsNullOrEmpty(staffId))
                {
                    officer = db.Officers.FirstOrDefault(o => o.StaffId == staffId);
                    if (officer == null)
                    {
                        officer = new Officer
                        {
                            Name = staffId,
                            StaffId = staffId,
                        };
                        db.Officers.Add(officer);
                        db.SaveChanges();
                    }
                }

                var company = companies.FirstOrDefault(c => c.Code == debt.CompanyCode);
                if(company == null)
                {
                    company = new Company
                    {
                        Name = debt.CompanyName,
                        Code = debt.CompanyCode,
                    };
                }

                // Update company if any change
                company.Officer = officer;
                company.NoOfEmployees = (int)debt.NoOfEmployees;
                company.LastDebtUpdate = importMonth;
                company.MonthsOfDebt = (int)debt.MonthsOfDebt;

                debt.Company = company;
            });
        }

        private void ReportProgress(int excelLoad, double rowPercent)
        {
            var rowPart = 100 - excelLoad;
            backgroundWorker1.ReportProgress((int)(excelLoad + rowPart*rowPercent));
        }

        private void CleanupAndQuit()
        {
            // restore cursor
            Cursor = Cursors.Default;
            Close();
        }

        private void OnImportError(bool imported, string errorMessage)
        {
            MessageBox.Show(errorMessage + (imported ? "\r\n\r\nDữ liệu có thể đã được import một phần và có thể bị trùng lắp khi bạn import lại." : ""));
            btnImport.Enabled = true;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog()
            {
                Filter = "Excel (*.xls)|*.xls|Excel 2003 (*.xlsx)|*.xlsx"
            };

            if (fileDialog.ShowDialog() != DialogResult.OK)
                return;

            txtImportFile.Text = fileDialog.FileName;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DoImport();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Value = 100;
            configReader.AddImportedMonth(importMonth);
            db.SaveChanges();

            // Close form
            MessageBox.Show(ErrorCount <= 0 ? "Import completed" : "Import completed with error(s)");
            CleanupAndQuit();
        }
    }
}
