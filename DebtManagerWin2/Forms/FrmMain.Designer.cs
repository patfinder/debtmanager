﻿namespace DebtManagerWin
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.danhSáchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cánBộToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.côngTyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cơQuanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loạiCôngVănToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chứngTừToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loạiNợToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.executeQuery = new System.Windows.Forms.ToolStripMenuItem();
            this.tácVụToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhậpDữLiệuNợToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhậpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.đônĐốcNợToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.xemDữLiệuNợToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inFormD04hToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nợKhóThuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.đônĐốcNợToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bànGiaoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xửLýNợCủaĐơnVịToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.danhSáchToolStripMenuItem,
            this.tácVụToolStripMenuItem,
            this.báoCáoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(426, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // danhSáchToolStripMenuItem
            // 
            this.danhSáchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cánBộToolStripMenuItem,
            this.côngTyToolStripMenuItem,
            this.cơQuanToolStripMenuItem,
            this.loạiCôngVănToolStripMenuItem,
            this.chứngTừToolStripMenuItem,
            this.loạiNợToolStripMenuItem,
            this.executeQuery});
            this.danhSáchToolStripMenuItem.Name = "danhSáchToolStripMenuItem";
            this.danhSáchToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.danhSáchToolStripMenuItem.Text = "Danh mục";
            // 
            // cánBộToolStripMenuItem
            // 
            this.cánBộToolStripMenuItem.Name = "cánBộToolStripMenuItem";
            this.cánBộToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.cánBộToolStripMenuItem.Text = "Cán bộ";
            this.cánBộToolStripMenuItem.Click += new System.EventHandler(this.cánBộToolStripMenuItem_Click);
            // 
            // côngTyToolStripMenuItem
            // 
            this.côngTyToolStripMenuItem.Name = "côngTyToolStripMenuItem";
            this.côngTyToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.côngTyToolStripMenuItem.Text = "Đơn vị/Công ty";
            this.côngTyToolStripMenuItem.Click += new System.EventHandler(this.côngTyToolStripMenuItem_Click);
            // 
            // cơQuanToolStripMenuItem
            // 
            this.cơQuanToolStripMenuItem.Name = "cơQuanToolStripMenuItem";
            this.cơQuanToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.cơQuanToolStripMenuItem.Text = "Cơ quan NN";
            this.cơQuanToolStripMenuItem.Click += new System.EventHandler(this.cơQuanToolStripMenuItem_Click);
            // 
            // loạiCôngVănToolStripMenuItem
            // 
            this.loạiCôngVănToolStripMenuItem.Name = "loạiCôngVănToolStripMenuItem";
            this.loạiCôngVănToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.loạiCôngVănToolStripMenuItem.Text = "Công văn";
            this.loạiCôngVănToolStripMenuItem.Click += new System.EventHandler(this.loạiCôngVănToolStripMenuItem_Click);
            // 
            // chứngTừToolStripMenuItem
            // 
            this.chứngTừToolStripMenuItem.Name = "chứngTừToolStripMenuItem";
            this.chứngTừToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.chứngTừToolStripMenuItem.Text = "Chứng từ";
            this.chứngTừToolStripMenuItem.Click += new System.EventHandler(this.chứngTừToolStripMenuItem_Click);
            // 
            // loạiNợToolStripMenuItem
            // 
            this.loạiNợToolStripMenuItem.Name = "loạiNợToolStripMenuItem";
            this.loạiNợToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.loạiNợToolStripMenuItem.Text = "Loại nợ";
            this.loạiNợToolStripMenuItem.Click += new System.EventHandler(this.loạiNợToolStripMenuItem_Click);
            // 
            // executeQuery
            // 
            this.executeQuery.Name = "executeQuery";
            this.executeQuery.Size = new System.Drawing.Size(155, 22);
            this.executeQuery.Text = "Excute Query";
            this.executeQuery.Click += new System.EventHandler(this.executeQuery_Click);
            // 
            // tácVụToolStripMenuItem
            // 
            this.tácVụToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nhậpDữLiệuNợToolStripMenuItem,
            this.nhậpToolStripMenuItem,
            this.đônĐốcNợToolStripMenuItem,
            this.toolStripMenuItem1,
            this.xemDữLiệuNợToolStripMenuItem});
            this.tácVụToolStripMenuItem.Name = "tácVụToolStripMenuItem";
            this.tácVụToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.tácVụToolStripMenuItem.Text = "Tác vụ";
            // 
            // nhậpDữLiệuNợToolStripMenuItem
            // 
            this.nhậpDữLiệuNợToolStripMenuItem.Name = "nhậpDữLiệuNợToolStripMenuItem";
            this.nhậpDữLiệuNợToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.nhậpDữLiệuNợToolStripMenuItem.Text = "Import dữ liệu nợ";
            this.nhậpDữLiệuNợToolStripMenuItem.Click += new System.EventHandler(this.nhậpDữLiệuNợToolStripMenuItem_Click_1);
            // 
            // nhậpToolStripMenuItem
            // 
            this.nhậpToolStripMenuItem.Name = "nhậpToolStripMenuItem";
            this.nhậpToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.nhậpToolStripMenuItem.Text = "Nhập nợ khó thu";
            this.nhậpToolStripMenuItem.Click += new System.EventHandler(this.nhậpNợKhóThuToolStripMenuItem_Click_1);
            // 
            // đônĐốcNợToolStripMenuItem
            // 
            this.đônĐốcNợToolStripMenuItem.Name = "đônĐốcNợToolStripMenuItem";
            this.đônĐốcNợToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.đônĐốcNợToolStripMenuItem.Text = "Đôn đốc nợ";
            this.đônĐốcNợToolStripMenuItem.Click += new System.EventHandler(this.đônĐốcNợToolStripMenuItem_Click_1);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(163, 6);
            // 
            // xemDữLiệuNợToolStripMenuItem
            // 
            this.xemDữLiệuNợToolStripMenuItem.Name = "xemDữLiệuNợToolStripMenuItem";
            this.xemDữLiệuNợToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.xemDữLiệuNợToolStripMenuItem.Text = "Xem dữ liệu nợ";
            this.xemDữLiệuNợToolStripMenuItem.Click += new System.EventHandler(this.xemDữLiệuNợToolStripMenuItem_Click);
            // 
            // báoCáoToolStripMenuItem
            // 
            this.báoCáoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inFormD04hToolStripMenuItem,
            this.nợKhóThuToolStripMenuItem,
            this.đônĐốcNợToolStripMenuItem1,
            this.bànGiaoToolStripMenuItem,
            this.xửLýNợCủaĐơnVịToolStripMenuItem});
            this.báoCáoToolStripMenuItem.Name = "báoCáoToolStripMenuItem";
            this.báoCáoToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.báoCáoToolStripMenuItem.Text = "Báo cáo";
            // 
            // inFormD04hToolStripMenuItem
            // 
            this.inFormD04hToolStripMenuItem.Name = "inFormD04hToolStripMenuItem";
            this.inFormD04hToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.inFormD04hToolStripMenuItem.Text = "In form D04h";
            this.inFormD04hToolStripMenuItem.Click += new System.EventHandler(this.inFormD04hToolStripMenuItem_Click);
            // 
            // nợKhóThuToolStripMenuItem
            // 
            this.nợKhóThuToolStripMenuItem.Name = "nợKhóThuToolStripMenuItem";
            this.nợKhóThuToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nợKhóThuToolStripMenuItem.Text = "Nợ khó thu";
            this.nợKhóThuToolStripMenuItem.Click += new System.EventHandler(this.nợKhóThuToolStripMenuItem_Click);
            // 
            // đônĐốcNợToolStripMenuItem1
            // 
            this.đônĐốcNợToolStripMenuItem1.Name = "đônĐốcNợToolStripMenuItem1";
            this.đônĐốcNợToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.đônĐốcNợToolStripMenuItem1.Text = "Đôn đốc nợ";
            this.đônĐốcNợToolStripMenuItem1.Click += new System.EventHandler(this.đônĐốcNợToolStripMenuItem1_Click);
            // 
            // bànGiaoToolStripMenuItem
            // 
            this.bànGiaoToolStripMenuItem.Name = "bànGiaoToolStripMenuItem";
            this.bànGiaoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.bànGiaoToolStripMenuItem.Text = "Bàn giao KTTN";
            this.bànGiaoToolStripMenuItem.Click += new System.EventHandler(this.bànGiaoToolStripMenuItem_Click);
            // 
            // xửLýNợCủaĐơnVịToolStripMenuItem
            // 
            this.xửLýNợCủaĐơnVịToolStripMenuItem.Name = "xửLýNợCủaĐơnVịToolStripMenuItem";
            this.xửLýNợCủaĐơnVịToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.xửLýNợCủaĐơnVịToolStripMenuItem.Text = "Xử lý nợ của đơn vị";
            this.xửLýNợCủaĐơnVịToolStripMenuItem.Click += new System.EventHandler(this.xửLýNợCủaĐơnVịToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 320);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.Text = "Chương trình quản lý nợ BHXH";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem danhSáchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cánBộToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem côngTyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cơQuanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loạiCôngVănToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tácVụToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhậpDữLiệuNợToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhậpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem đônĐốcNợToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inFormD04hToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem đônĐốcNợToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bànGiaoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xửLýNợCủaĐơnVịToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chứngTừToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem executeQuery;
        private System.Windows.Forms.ToolStripMenuItem nợKhóThuToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem xemDữLiệuNợToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loạiNợToolStripMenuItem;
    }
}