﻿using DebtManagerWin.DataServices;
using DebtManagerWin.Forms;
using DebtManagerWin.Forms.Reports;
using DebtManagerWin.Models;
using DebtManagerWin.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin
{
    public partial class FrmMain : Form
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));
        private DebtDbContext db => Program.db;

        public FrmMain()
        {
            InitializeComponent();

            WindowState = FormWindowState.Maximized;
            //WindowState = FormWindowState.Normal;
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            // Test menu calls
            //Invoke((Action<object, EventArgs>)loạiCôngVănToolStripMenuItem_Click, new object[] { null, null });
            //OpenForm<ListViewGroupsExample>();
        }

        // ========================== Form Handlers ==========================

        // ======================
        // Form handling - How it works?
        // - OpenForm: receive param ShowFormWithParamsFunc (which call FormUtils.UpdateXXX)
        //          passing Close-hanlder which save the changes.
        // - FormUtils.UpdateXXX: This func show FrmUpdateCatalog down the stream which receive params: DBSet, format of item and hanlder to close form.
        // ======================

        #region Form Handlers
        private static Dictionary<string, Form> _FormList = new Dictionary<string, Form>();
        
        private void OpenForm<F>(string formId = null) where F : Form, new()
        {
            var formType = typeof(F);
            if (string.IsNullOrWhiteSpace(formId))
                formId = formType.FullName;

            if (!_FormList.ContainsKey(formId))
            {
                var form = FormUtils.ShowMdiChild<F>(this, () => RemoveForm(formId));
                _FormList.Add(formId, form);
            }
            else
            {
                _FormList[formId].Activate();
            }
        }

        /// <summary>
        /// A delegate that handle showing and closing the form
        /// On form close, execute actions like updating DB.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="db">Db object; Use to update Db on commit.</param>
        /// <param name="closeAction">A callback to do final clean-up (like remove reference to this form.)</param>
        /// <param name="createArgs"></param>
        /// <returns></returns>
        private delegate Form ShowFormWithParamsFunc(Form parent, DebtDbContext db, Action<object, FormClosedEventArgs> closeAction, params object[] createArgs);
        
        /// <summary>
        /// Wrapper to activate existing form (if opened before) or launch a new form.
        /// </summary>
        /// <param name="showForm"></param>
        /// <param name="formId"></param>
        /// <param name="args"></param>
        private void OpenForm(ShowFormWithParamsFunc showForm, string formId = null, params object[] args)
        {
            if (string.IsNullOrWhiteSpace(formId))
                formId = showForm.Method.Name;

            if (!_FormList.ContainsKey(formId))
            {
                var form = showForm(this, db, (a, b) => RemoveForm(formId), args);
                _FormList.Add(formId, form);
            }
            else
            {
                _FormList[formId].Activate();
            }
        }

        private static void RemoveForm(string formId)
        {
            if (_FormList.ContainsKey(formId))
            {
                _FormList.Remove(formId);
            }
        }
        #endregion // Form Handlers

        // ========================== Catalogs ==========================

        #region Catalogs
        private void cánBộToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm((parent, db, closeAction, d) => FormUtils.UpdateOfficer(parent, db, closeAction));
        }

        private void côngTyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm((parent, db, closeAction, d) => FormUtils.UpdateCompany(parent, db, closeAction));

            //OpenForm((parent, db, closeAction, d) => {
            //    try
            //    {
            //        return FormUtils.UpdateCompany(parent, db, closeAction);
            //    }
            //    catch(Exception ex)
            //    {
            //        _logger.Error($"{nameof(côngTyToolStripMenuItem_Click)} error: {ex.Message} - {ex.StackTrace}");
            //        return null;
            //    }
            //});
        }

        private void cơQuanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm((parent, db, closeAction, d) => FormUtils.UpdateDepartment(parent, db, closeAction));
        }


        private ShowFormWithParamsFunc _AddPaperType = (parent, db, closeAction, params_) =>
        {
            return FormUtils.UpdatePaperType(parent, db, (sender, eventArgs) => closeAction(sender, eventArgs), (PaperGroup)params_[0]);
        };

        private void loạiCôngVănToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var paperGroup = PaperGroup.CompanyNotification;
            var formId = $"{nameof(FormUtils.UpdatePaperType)}-{paperGroup}";
            OpenForm(_AddPaperType, formId, paperGroup);
        }

        private void chứngTừToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var paperGroup = PaperGroup.DebtReminding;
            var formId = $"{nameof(FormUtils.UpdatePaperType)}-{paperGroup}";
            OpenForm(_AddPaperType, formId, paperGroup);
        }

        private void loạiNợToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm((parent, db, closeAction, d) => FormUtils.UpdateDebtType(parent, db, closeAction));
        }

        #endregion

        // ========================== Tasks ==========================

        #region Tasks

        private void nhậpDữLiệuNợToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            OpenForm<FrmImportDebtData>();
        }

        private void nhậpNợKhóThuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            OpenForm<FrmUpdateBadDebt>();
        }

        private void đônĐốcNợToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            OpenForm<FrmUpdateRemindDebt>();
        }

        private void xemDữLiệuNợToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm<FrmViewDebt>();
        }
        #endregion

        // ========================== Reports ==========================

        #region Reports
        private void inFormD04hToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm<FrmD04hReport>();
        }

        /// <summary>
        /// Show form handler
        /// </summary>
        private ShowFormWithParamsFunc _reportFormsHandler = (parent, db, closeAction, params_) =>
        {
            var reportType = (DebtReportType)params_[0];
            var sendToKttn = params_.Length > 1 ? (bool)params_[1] : false;

            var form = new FrmDebtReport(reportType, sendToKttn)
            {
                MdiParent = parent
            };

            if (closeAction != null)
                form.FormClosed += new FormClosedEventHandler(closeAction);

            form.Show();

            return form;
        };

        private void nợKhóThuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var reportType = DebtReportType.BadDebtReport;
            var formId = $"{nameof(FrmDebtReport)}-{reportType}";
            OpenForm(_reportFormsHandler, formId, reportType);
        }

        private void đônĐốcNợToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var reportType = DebtReportType.DebtRemindReport;
            var formId = $"{nameof(FrmDebtReport)}-{reportType}-kttn-{false}";
            OpenForm(_reportFormsHandler, formId, reportType, false);
        }

        private void bànGiaoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var reportType = DebtReportType.DebtRemindReport;
            var formId = $"{nameof(FrmDebtReport)}-{reportType}-kttn-{true}";
            OpenForm(_reportFormsHandler, formId, reportType, true);
        }

        T DefaultShowFormFunc<T>(Form parent, DebtDbContext db, Action<object, FormClosedEventArgs> closeAction, object[] p) where T : Form, new()
        {
            var form = new T()
            {
                MdiParent = parent
            };

            if (closeAction != null)
                form.FormClosed += new FormClosedEventHandler(closeAction);

            form.Show();

            return form;
        }
        
        private void xửLýNợCủaĐơnVịToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenForm((parent, db, closeAction, params_) => DefaultShowFormFunc<FrmCompanyDebtDetailReport>(parent, db, closeAction, params_));
        }
        
        private void executeQuery_Click(object sender, EventArgs e)
        {
            OpenForm((parent, db, closeAction, params_) => DefaultShowFormFunc<FrmExecuteQuery>(parent, db, closeAction, params_));
        }

        private void testToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            //dynamic s;

            //((dynamic)e).haha = 1;

            Func<object, int> getId = (obj) => ((dynamic)obj).Id;

            Action<object, int> setId = (obj, val) => ((dynamic)obj).Id = val;

            var company = db.Companies.First();
            setId(company, 1000);
            var id = db.Companies.ToList().FirstOrDefault(c => getId(c) == 1);
            Console.WriteLine($"Company Id: {id}");
        }
        #endregion

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            //if (keyData == (Keys.Control | Keys.F))
            //{
            //    MessageBox.Show("What the Ctrl+F?");
            //    return true;
            //}

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
