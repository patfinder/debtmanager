﻿namespace DebtManagerWin
{
    partial class FrmUpdateBadDebt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOfficer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtPaperDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPaperNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboPaperType = new System.Windows.Forms.ComboBox();
            this.cboDepartment = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtBadDebtMonth = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.lvwBadDebts = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cboBadDebtType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.updUnpaidEmployeeCount = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.updUnpaidEmployeeCount)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã đơn vị";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(163, 28);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(131, 20);
            this.txtCompany.TabIndex = 1;
            this.txtCompany.TextChanged += new System.EventHandler(this.txtCompany_TextChanged);
            this.txtCompany.Leave += new System.EventHandler(this.txtCompany_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cán bộ chuyên quản";
            // 
            // txtOfficer
            // 
            this.txtOfficer.Location = new System.Drawing.Point(163, 70);
            this.txtOfficer.Name = "txtOfficer";
            this.txtOfficer.ReadOnly = true;
            this.txtOfficer.Size = new System.Drawing.Size(259, 20);
            this.txtOfficer.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ngày chứng từ";
            // 
            // dtPaperDate
            // 
            this.dtPaperDate.CustomFormat = "dd/MM/yyyy";
            this.dtPaperDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPaperDate.Location = new System.Drawing.Point(163, 109);
            this.dtPaperDate.Name = "dtPaperDate";
            this.dtPaperDate.Size = new System.Drawing.Size(131, 20);
            this.dtPaperDate.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(52, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Số chứng từ";
            // 
            // txtPaperNumber
            // 
            this.txtPaperNumber.Location = new System.Drawing.Point(163, 151);
            this.txtPaperNumber.Name = "txtPaperNumber";
            this.txtPaperNumber.Size = new System.Drawing.Size(131, 20);
            this.txtPaperNumber.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(457, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Loại chứng từ";
            // 
            // cboPaperType
            // 
            this.cboPaperType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPaperType.FormattingEnabled = true;
            this.cboPaperType.Location = new System.Drawing.Point(562, 28);
            this.cboPaperType.Name = "cboPaperType";
            this.cboPaperType.Size = new System.Drawing.Size(215, 21);
            this.cboPaperType.TabIndex = 12;
            // 
            // cboDepartment
            // 
            this.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDepartment.FormattingEnabled = true;
            this.cboDepartment.Location = new System.Drawing.Point(562, 70);
            this.cboDepartment.Name = "cboDepartment";
            this.cboDepartment.Size = new System.Drawing.Size(215, 21);
            this.cboDepartment.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(457, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Cơ quan ban hành";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(457, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 32);
            this.label7.TabIndex = 17;
            this.label7.Text = "Tháng xác định nợ khó thu";
            // 
            // dtBadDebtMonth
            // 
            this.dtBadDebtMonth.CustomFormat = "MM/yyyy";
            this.dtBadDebtMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtBadDebtMonth.Location = new System.Drawing.Point(562, 109);
            this.dtBadDebtMonth.Name = "dtBadDebtMonth";
            this.dtBadDebtMonth.Size = new System.Drawing.Size(131, 20);
            this.dtBadDebtMonth.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(52, 271);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Các đơn vị thêm gần đây";
            // 
            // lvwBadDebts
            // 
            this.lvwBadDebts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvwBadDebts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvwBadDebts.FullRowSelect = true;
            this.lvwBadDebts.HideSelection = false;
            this.lvwBadDebts.Location = new System.Drawing.Point(55, 299);
            this.lvwBadDebts.MultiSelect = false;
            this.lvwBadDebts.Name = "lvwBadDebts";
            this.lvwBadDebts.Size = new System.Drawing.Size(747, 256);
            this.lvwBadDebts.TabIndex = 26;
            this.lvwBadDebts.UseCompatibleStateImageBehavior = false;
            this.lvwBadDebts.View = System.Windows.Forms.View.Details;
            this.lvwBadDebts.SelectedIndexChanged += new System.EventHandler(this.lvwBadDebts_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Đơn vị";
            this.columnHeader1.Width = 180;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Cơ quan ban hành";
            this.columnHeader2.Width = 180;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Ngày";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Số chứng từ";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Loại nợ";
            this.columnHeader5.Width = 180;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(55, 208);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 22;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(163, 208);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 23;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(274, 208);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 24;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.Location = new System.Drawing.Point(163, 51);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(9, 13);
            this.lblCompanyName.TabIndex = 3;
            this.lblCompanyName.Text = "|";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(52, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Tên đơn vị";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(457, 189);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Loại nợ khó thu";
            // 
            // cboBadDebtType
            // 
            this.cboBadDebtType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBadDebtType.FormattingEnabled = true;
            this.cboBadDebtType.Location = new System.Drawing.Point(562, 185);
            this.cboBadDebtType.Name = "cboBadDebtType";
            this.cboBadDebtType.Size = new System.Drawing.Size(215, 21);
            this.cboBadDebtType.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(457, 137);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 34);
            this.label11.TabIndex = 9;
            this.label11.Text = "Số lao động chưa giải quyết chế độ";
            // 
            // updUnpaidEmployeeCount
            // 
            this.updUnpaidEmployeeCount.Location = new System.Drawing.Point(562, 150);
            this.updUnpaidEmployeeCount.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.updUnpaidEmployeeCount.Name = "updUnpaidEmployeeCount";
            this.updUnpaidEmployeeCount.Size = new System.Drawing.Size(131, 20);
            this.updUnpaidEmployeeCount.TabIndex = 27;
            // 
            // FrmUpdateBadDebt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 572);
            this.Controls.Add(this.updUnpaidEmployeeCount);
            this.Controls.Add(this.lblCompanyName);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lvwBadDebts);
            this.Controls.Add(this.dtBadDebtMonth);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cboBadDebtType);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cboDepartment);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPaperNumber);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dtPaperDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtOfficer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboPaperType);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Name = "FrmUpdateBadDebt";
            this.Text = "Cập nhật nợ khó thu";
            ((System.ComponentModel.ISupportInitialize)(this.updUnpaidEmployeeCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOfficer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtPaperDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPaperNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboPaperType;
        private System.Windows.Forms.ComboBox cboDepartment;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtBadDebtMonth;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListView lvwBadDebts;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboBadDebtType;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown updUnpaidEmployeeCount;
    }
}