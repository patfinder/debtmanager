﻿using DebtManagerWin.DataServices;
using DebtManagerWin.Models;
using DebtManagerWin.Services;
using DebtManagerWin.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin
{
    /// <summary>
    /// Cập nhật nợ khó thu.
    /// </summary>
    public partial class FrmUpdateBadDebt : Form
    {
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(Program));
        private DebtDbContext db => Program.db;

        public Company CurrentCompany { get; set; }

        public FrmUpdateBadDebt()
        {
            // ==================================================================
            // NOTE: 
            //      * lvwBadDebts should be considered root of events chain. Changes to the list selection should be strictly controlled.
            //        Ie: special care on deselecting list
            // ==================================================================

            InitializeComponent();

            try
            {
                //OwnerDraw mode must set
                lvwBadDebts.DrawColumnHeader += LvwBadDebts_DrawColumnHeader;

                // Paper Type
                cboPaperType.Items.AddRange(db.PaperTypes.Where(t => t.PaperGroup == PaperGroup.CompanyNotification).ToArray());
                cboPaperType.DisplayMember = nameof(PaperType.DisplayName);

                // Departments
                cboDepartment.Items.AddRange(db.Departments.ToArray());
                cboDepartment.DisplayMember = nameof(Department.DisplayName);

                // DebtTypes
                cboBadDebtType.Items.AddRange(db.BadDebtTypes.ToArray());
                cboBadDebtType.DisplayMember = nameof(BadDebtType.DisplayName);

                LoadBadDebtList();
            }
            catch (Exception ex)
            {
                _Logger.Error($"{nameof(FrmUpdateBadDebt)} - Exception: {ex}");
            }
        }
        
        private void LvwBadDebts_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.GreenYellow, e.Bounds);
            e.DrawText();
        }

        private void LoadBadDebtList()
        {
            lvwBadDebts.Items.Clear();

            var lvItems = new List<ListViewItem>();
            db.GovernmentalDocuments.OrderByDescending(d => d.PaperDate).Include(d => d.Company).Take(AppConstants.RowsToDisplay).ToList().ForEach(item =>
            {
                var lvItem = new ListViewItem
                {
                    Text = item.Company.Name,
                    Tag = item
                };
                lvItem.SubItems.AddRange(new string[] {
                    item.Department?.Name,
                    item.PaperDate.ToString(AppConstants.DateFormat),
                    item.PaperNumber,
                    item.BadDebtType.Name,
                });
                lvItems.Add(lvItem);
            });

            lvwBadDebts.Items.AddRange(lvItems.ToArray());
        }


        private void ShowSelectedBadDebt()
        {
            var badDebt = lvwBadDebts.SelectedItems.Count <= 0 ? null : lvwBadDebts.SelectedItems[0].Tag as GovernmentalDocument;

            if (badDebt == null)
            {
                dtPaperDate.Value = AppConstants.MinDate;
                txtPaperNumber.Text = "";
                cboPaperType.SelectedItem = null;
                cboDepartment.SelectedItem = null;
                cboBadDebtType.SelectedItem = null;
                dtBadDebtMonth.Value = AppConstants.MinDate;

                return;
            }

            dtPaperDate.Value = badDebt.PaperDate;
            txtPaperNumber.Text = badDebt.PaperNumber;
            cboPaperType.SelectedItem = badDebt.PaperType;
            cboDepartment.SelectedItem = badDebt.Department;
            cboBadDebtType.SelectedItem = badDebt.BadDebtType;
            dtBadDebtMonth.Value = badDebt.DebtMonth;
        }

        /// <summary>
        /// Called when entered company code changed
        /// </summary>
        private void UpdateCompanyDependentFields(bool updateList = false)
        {
            try
            {
                // Clear current debt
                if (updateList && _TriggerDebtListUpdate && lvwBadDebts.SelectedItems.Count > 0)
                {
                    lvwBadDebts.SelectedItems.Clear();
                }

                if (CurrentCompany == null)
                {
                    lblCompanyName.Text = "";
                    txtOfficer.Text = "";
                    return;
                }

                lblCompanyName.Text = CurrentCompany.Name;
                txtOfficer.Text = CurrentCompany.Officer?.Name;
            }
            catch (Exception ex)
            {
                _Logger.Error($"{nameof(UpdateCompanyDependentFields)} - Exception: {ex}");

                MessageBox.Show($"Lỗi: {ex.Message}");
            }
        }

        private void txtCompany_Leave(object sender, EventArgs e)
        {
            // Update dependent field when company field lost focus
            if (string.IsNullOrWhiteSpace(txtCompany.Text))
                CurrentCompany = null;
            else
                CurrentCompany = db.Companies.FirstOrDefault(c => c.Code == txtCompany.Text);

            UpdateCompanyDependentFields(false);
        }

        private List<string> ValidateInputs()
        {
            var errors = new List<string>();

            if (string.IsNullOrWhiteSpace(txtCompany.Text))
            {
                errors.Add("Bạn hãy nhập mã công ty");
            }
            else
            {
                if (dtPaperDate.Value < AppConstants.ValidDate || dtPaperDate.Value > DateTime.Now)
                    errors.Add("Ngày chứng từ không hợp lệ");

                if (string.IsNullOrWhiteSpace(txtPaperNumber.Text))
                    errors.Add("Số chứng từ không hợp lệ");

                if (cboPaperType.SelectedIndex < 0)
                    errors.Add("Bạn hãy chọn loại chứng từ");

                if (cboDepartment.SelectedIndex < 0)
                    errors.Add("Bạn hãy chọn cơ quan ban hành chứng từ");

                if (dtBadDebtMonth.Value < AppConstants.ValidDate || dtBadDebtMonth.Value > DateTime.Now)
                    errors.Add("Tháng xác định nợ khó thu không hợp lệ");

                if (cboBadDebtType.SelectedIndex < 0)
                    errors.Add("Bạn hãy chọn loại nợ khó thu");
            }

            return errors;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Clear selected item
            lvwBadDebts.SelectedIndices.Clear();
        }

        private void SaveChanges(GovernmentalDocument badDebt)
        {
            var errors = ValidateInputs();
            if (errors.Any())
            {
                MessageBox.Show(string.Join("\r\n", errors), "Lỗi");
                return;
            }

            var debtMonth = new DateTime(dtBadDebtMonth.Value.Year, dtBadDebtMonth.Value.Month, 1);
            var now = DateTime.Now;

            if (badDebt == null)
            {
                badDebt = new GovernmentalDocument
                {
                    CreatedTime = now,
                    UpdateTime = now,
                    CompanyId = (int)CurrentCompany.Id,
                    OfficerId = CurrentCompany.Officer?.Id,
                    PaperDate = dtPaperDate.Value,
                    PaperNumber = txtPaperNumber.Text,
                    PaperTypeId = (int)(cboPaperType.SelectedItem as PaperType).Id,
                    UnpaidEmployeeCount = (int)updUnpaidEmployeeCount.Value,
                    DepartmentId = (int)(cboDepartment.SelectedItem as Department).Id,
                    BadDebtTypeId = (int)(cboBadDebtType.SelectedItem as BadDebtType).Id,
                    DebtMonth = debtMonth,
                };

                db.GovernmentalDocuments.Add(badDebt);
            }
            else
            {
                //CreatedTime = now;
                badDebt.UpdateTime = now;
                //badDebt.CompanyId = (int)company.Id;
                //badDebt.OfficerId = CurrentCompany.Officer?.Id ?? 0;
                badDebt.PaperDate = dtPaperDate.Value;
                badDebt.PaperNumber = txtPaperNumber.Text;
                badDebt.PaperTypeId = (int)(cboPaperType.SelectedItem as PaperType).Id;
                badDebt.UnpaidEmployeeCount = (int)updUnpaidEmployeeCount.Value;
                badDebt.DepartmentId = (int)(cboDepartment.SelectedItem as Department).Id;
                badDebt.BadDebtTypeId = (int)(cboBadDebtType.SelectedItem as BadDebtType).Id;
                badDebt.DebtMonth = debtMonth;
            }

            db.SaveChanges();

            // Reload bad-debt list
            LoadBadDebtList();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //if (lvwBadDebts.SelectedItems.Count <= 0)
            //{
            //    MessageBox.Show("Bạn hãy chọn 1 dòng từ danh sách bên dưới để cập nhật");
            //    return;
            //}

            SaveChanges(lvwBadDebts.SelectedIndices.Count > 0 ? lvwBadDebts.SelectedItems[0].Tag as GovernmentalDocument : null);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvwBadDebts.SelectedItems.Count <= 0)
                return;

            var badDebt = lvwBadDebts.SelectedItems[0].Tag as GovernmentalDocument;
            db.GovernmentalDocuments.Remove(badDebt);
            db.SaveChanges();

            // Reload bad-debt list
            LoadBadDebtList();
        }

        private bool _TriggerDebtListUpdate = true;

        private void lvwBadDebts_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Prevent Trigger company update
                _TriggerDebtListUpdate = false;

                if (lvwBadDebts.SelectedItems.Count <= 0)
                {
                    txtCompany.Text = "";
                }
                else
                {
                    var badDebt = (GovernmentalDocument)lvwBadDebts.SelectedItems[0].Tag;
                    txtCompany.Text = badDebt.Company.Code;
                }

                txtCompany_Leave(null, null);
            }
            catch (Exception ex)
            {
                _Logger.Error($"{nameof(lvwBadDebts_SelectedIndexChanged)} - Error: {ex}");
            }
            finally
            {
                _TriggerDebtListUpdate = true;
            }

            ShowSelectedBadDebt();
        }

        private void txtCompany_TextChanged(object sender, EventArgs e)
        {
            // reset current company
            if (CurrentCompany != null)
            {
                CurrentCompany = null;
                UpdateCompanyDependentFields(true);
            }
        }
    }
}
