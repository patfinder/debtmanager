﻿using DebtManagerWin.Utils;
using log4net;
using SmartDev.Libs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace DebtManagerWin
{
    /// <summary>
    /// A generic form for updating catalogs.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    //public partial class FrmUpdateCatalog : CallbackableForm
    public partial class FrmUpdateCatalog<T> : CallbackableForm<T> where T : class
    {
        private static readonly ILog _logger = LogManager.GetLogger($"FrmUpdateCatalog-{typeof(T)}");

        //private DbSet DbSet = null;
        private DbSet<T> DbSet = null;
        Func<int, string> MoreInfo = null;
        Func<int, bool> CanDelete = null;

        private string ObjectType = "Phần tử";
        private int FieldCount = 1;

        private ListViewItem CurrentItem { get; set; }
        private bool FieldsChanged { get; set; }

        public int ColumnWidth { get { return 270; } }

        public List<DanhMucItem> ItemList { get; private set; }

        /// <summary>
        /// Form cập nhật danh mục
        /// </summary>
        /// <param name="objectType">Loại đối tượng danh mục</param>
        /// <param name="canDelete">Method for checking if an item can be deleted.</param>
        /// <param name="items"></param>
        //public FrmUpdateCatalog(string objectType, DbSet dbSet, List<DanhMucItem> items, string[] fieldNames)
        public FrmUpdateCatalog(string objectType, DbSet<T> dbSet, Func<int, string> moreInfo, Func<int, bool> canDelete, List<DanhMucItem> items, string[] fieldNames)
        {
            if (fieldNames == null || fieldNames.Length == 0)
            {
                throw new InvalidOperationException($"{nameof(fieldNames)} cannot be empty");
            }

            DbSet = dbSet;
            MoreInfo = moreInfo;
            CanDelete = canDelete;
            ObjectType = objectType;
            ItemList = items;
            FieldCount = fieldNames.Length;

            InitializeComponent();

            // Disable delete button
            btnDelete.Enabled = CanDelete != null && CanDelete(0);

            Text = $"Cập nhật {ObjectType}";
            lblItemList.Text = $"Danh sách {ObjectType}";

            lvwItems.DrawColumnHeader += LvwItems_DrawColumnHeader;

            // TODO: improve this.
            // Disable btn Add, Delete for "Company" form
            var companyCatalogName = AttributeUtils.GetDisplayName<Models.Company>();
            if (objectType == companyCatalogName)
            {
                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
            }

            // Hide unused fields
            AllLabels.Skip(FieldCount).ToList().ForEach(l => l.Visible = false);
            AllFields.Skip(FieldCount).ToList().ForEach(l => l.Visible = false);

            // Update field labels
            FieldRange.ForEach(i => Labels[i].Text = fieldNames[i]);

            // Add lv columns
            fieldNames.ToList().ForEach(f => lvwItems.Columns.Add(f).Width = ColumnWidth);

            // Add items to lv
            ItemList.ForEach(item =>
            {
                var lvItem = new ListViewItem
                {
                    Tag = item,
                    Text = item.Id.ToString(),
                };

                lvItem.SubItems.AddRange(item.Fields);
                lvwItems.Items.Add(lvItem);
            });

            Fields.ForEach(f => f.TextChanged += Field_TextChanged);

            // Form event handler
            //this.OnFormCloseOk += FrmUpdateCatalog_OnFormCloseOk;
            this.FormClosing += FrmUpdateCatalog_FormClosing;
        }

        private void LvwItems_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.GreenYellow, e.Bounds);
            e.DrawText();
        }

        private void Field_TextChanged(object sender, EventArgs e)
        {
            FieldsChanged = true;
        }

        private List<int> FieldRange
        {
            get
            {
                return Enumerable.Range(0, FieldCount).ToList();
            }
        }

        private List<Label> _labels;
        private List<Label> AllLabels
        {
            get
            {
                if (_labels == null)
                {
                    _labels = new List<Label> {
                        lblField1, lblField2, lblField3
                    };
                }

                return _labels;
            }
        }
        private List<Label> Labels => AllLabels.Take(FieldCount).ToList();

        private List<TextBox> _fields;
        private List<TextBox> AllFields
        {
            get
            {
                if (_fields == null)
                {
                    _fields = new List<TextBox> {
                        txtField1, txtField2, txtField3
                    };
                }

                return _fields;
            }
        }
        private List<TextBox> Fields => AllFields.Take(FieldCount).ToList();

        private void btnAdd_Click(object sender, EventArgs e)
        {
            lvwItems.SelectedIndices.Clear();
            //Fields.ForEach(f => f.Text = "");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges(lvwItems.SelectedItems.Count > 0 ? lvwItems.SelectedItems[0] : null);
            FieldsChanged = false;
        }

        private void SaveChanges(ListViewItem lvItem = null)
        {
            DanhMucItem item;

            // Add new
            if (lvItem == null)
            {
                item = new DanhMucItem
                {
                    Status = DanhMucItemStatus.Updated,
                    Fields = Fields.Select(f => f.Text).ToArray()
                };
                // Add to returned list
                ItemList.Add(item);

                lvItem = new ListViewItem
                {
                    Tag = item,
                    Text = "<new>",
                };

                lvItem.SubItems.AddRange(item.Fields);
                lvwItems.Items.Add(lvItem);

                return;
            }

            // Update selected item
            var newValues = Fields.Select(f => f.Text).ToArray();

            //lvItem = lvwItems.SelectedItems[0];
            lvItem.SubItems.Clear();
            lvItem.Text = txtId.Text;
            lvItem.SubItems.AddRange(newValues);

            item = lvItem.Tag as DanhMucItem;
            item.Status = DanhMucItemStatus.Updated;
            item.Fields = newValues;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvwItems.SelectedItems.Count <= 0)
            {
                MessageBox.Show($"Bạn hãy chọn 1 '{ObjectType}' để xóa.");
                return;
            }

            var lvItem = lvwItems.SelectedItems[0];
            var item = lvItem.Tag as DanhMucItem;

            // New item, delete
            if (item.Id == 0)
            {
                ItemList.Remove(item);
            }
            else
            {
                // Check if item deletable
                //if (CanDelete != null && !CanDelete(item.Id))
                //{
                //    MessageBox.Show($"Bạn không thể xóa '{ObjectType}' này.");
                //    return;
                //}

                // Mark deleted
                item.Status = DanhMucItemStatus.Deleted;
            }

            // Remove from lv
            lvItem.Remove();
        }

        private void lvwItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Warning if fields changes
            if (FieldsChanged &&
                CurrentItem != (lvwItems.SelectedItems.Count == 0 ? null : lvwItems.SelectedItems[0]))
            {
                var response = MessageBox.Show("Bạn chưa lưu thay đổi. Bạn có muốn lưu lại không?", "Cảnh báo", MessageBoxButtons.YesNoCancel);

                // Cancel selection
                if (response == DialogResult.Cancel)
                {
                    lvwItems.SelectedItems.Clear();
                    if (CurrentItem != null)
                    {
                        CurrentItem.Selected = true;
                        return;
                    }
                }
                else if (response == DialogResult.Yes)
                {
                    // Save change before go ahead
                    SaveChanges(CurrentItem);
                }
            }

            // Set changes flag
            if (lvwItems.SelectedIndices.Count <= 0)
                CurrentItem = null;
            else
                CurrentItem = lvwItems.SelectedItems[0];

            txtId.Text = CurrentItem?.Text;
            FieldRange.ForEach(i => {
                Fields[i].Text = CurrentItem?.SubItems[i + 1].Text;
            });
            try
            {
                txtNotes.Text = CurrentItem == null || MoreInfo == null ? "" : MoreInfo(int.Parse(CurrentItem.Text));
            }
            catch(Exception ex)
            {
                _logger.Error($"{nameof(FrmUpdateCatalog<T>)}.{nameof(lvwItems_SelectedIndexChanged)} error: " + 
                    ex.ToString());
            }
            FieldsChanged = false;
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            // Save changes
            CommitChanges();

            DialogResult = DialogResult.OK;
            Close();
        }

        private void CommitChanges()
        {
            _onFormCloseOk?.Invoke(DbSet);
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            if (QuitHandler())
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
        }

        /// <summary>
        /// Handle checking before QUITING form.
        /// </summary>
        /// <returns>True: go ahead. False: cancel action or on error.</returns>
        private bool QuitHandler()
        {
            // Check if changes made: New or Changed
            if (ItemList.Any(i => i.Status == DanhMucItemStatus.Updated || i.Status == DanhMucItemStatus.Deleted))
            {
                var response = MessageBox.Show("Bạn có muốn lưu những thay đổi trước khi thoát không?", "Cảnh báo", MessageBoxButtons.YesNoCancel);

                if (response == DialogResult.Yes)
                {
                    try
                    {
                        // Save changes before go ahead
                        CommitChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Lỗi: {ex.Message}");
                        _logger.Error($"{nameof(btnQuit_Click)} - Exception: {ex}");
                        return false;
                    }
                }
                else if (response == DialogResult.Cancel)
                {
                    // Canncel current action.
                    return false;
                }
            }

            // Skip changes (if any) and Go ahead.
            return true;
        }

        private void FrmUpdateCatalog_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Check not from quit button.
            if ((sender as Form).ActiveControl?.Text != btnQuit.Text &&
                (sender as Form).ActiveControl?.Text != btnFinish.Text)
            {
                // User trigger, check saving
                if (e.CloseReason == CloseReason.UserClosing && !QuitHandler())
                {
                    e.Cancel = true;
                }
            }
        }


        //private void btnAdd_Click(object sender, EventArgs e) {}
        //private void btnDelete_Click(object sender, EventArgs e){}
        //private void lvwItems_SelectedIndexChanged(object sender, EventArgs e) { }
        //private void btnSave_Click(object sender, EventArgs e) { }
        //private void btnFinish_Click(object sender, EventArgs e) { }
        //private void btnQuit_Click(object sender, EventArgs e) { }
    }

    public enum DanhMucItemStatus
    {
        /// <summary>
        /// Existing and unchanged item
        /// </summary>
        Existing,
        /// <summary>
        /// Newly created or updated item
        /// </summary>
        Updated,
        Deleted,
    }

    public class DanhMucItem
    {
        public int Id;
        public string[] Fields;
        public DanhMucItemStatus Status = DanhMucItemStatus.Existing;
    }
}
