﻿namespace DebtManagerWin
{
    partial class FrmUpdateRemindDebt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOfficer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtPaperDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPaperNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboPaperType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lvwDebtReminds = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.chkSendToKttn = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblLastRemind = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã đơn vị";
            // 
            // txtCompany
            // 
            this.txtCompany.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCompany.Location = new System.Drawing.Point(163, 28);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(259, 20);
            this.txtCompany.TabIndex = 1;
            this.txtCompany.TextChanged += new System.EventHandler(this.txtCompany_TextChanged);
            this.txtCompany.Leave += new System.EventHandler(this.txtCompany_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cán bộ chuyên quản";
            // 
            // txtOfficer
            // 
            this.txtOfficer.Location = new System.Drawing.Point(163, 70);
            this.txtOfficer.Name = "txtOfficer";
            this.txtOfficer.ReadOnly = true;
            this.txtOfficer.Size = new System.Drawing.Size(259, 20);
            this.txtOfficer.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ngày chứng từ";
            // 
            // dtPaperDate
            // 
            this.dtPaperDate.CustomFormat = "dd/MM/yyyy";
            this.dtPaperDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPaperDate.Location = new System.Drawing.Point(166, 125);
            this.dtPaperDate.Name = "dtPaperDate";
            this.dtPaperDate.Size = new System.Drawing.Size(131, 20);
            this.dtPaperDate.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(52, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Số chứng từ";
            // 
            // txtPaperNumber
            // 
            this.txtPaperNumber.Location = new System.Drawing.Point(163, 151);
            this.txtPaperNumber.Name = "txtPaperNumber";
            this.txtPaperNumber.Size = new System.Drawing.Size(131, 20);
            this.txtPaperNumber.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(457, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Loại chứng từ";
            // 
            // cboPaperType
            // 
            this.cboPaperType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPaperType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPaperType.FormattingEnabled = true;
            this.cboPaperType.Location = new System.Drawing.Point(580, 28);
            this.cboPaperType.Name = "cboPaperType";
            this.cboPaperType.Size = new System.Drawing.Size(231, 21);
            this.cboPaperType.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(52, 271);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Các đơn vị thêm gần đây";
            // 
            // lvwDebtReminds
            // 
            this.lvwDebtReminds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvwDebtReminds.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader6,
            this.columnHeader5});
            this.lvwDebtReminds.FullRowSelect = true;
            this.lvwDebtReminds.HideSelection = false;
            this.lvwDebtReminds.Location = new System.Drawing.Point(55, 299);
            this.lvwDebtReminds.MultiSelect = false;
            this.lvwDebtReminds.Name = "lvwDebtReminds";
            this.lvwDebtReminds.Size = new System.Drawing.Size(756, 271);
            this.lvwDebtReminds.TabIndex = 26;
            this.lvwDebtReminds.UseCompatibleStateImageBehavior = false;
            this.lvwDebtReminds.View = System.Windows.Forms.View.Details;
            this.lvwDebtReminds.SelectedIndexChanged += new System.EventHandler(this.lvwDebtReminds_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Đơn vị";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Ngày chứng từ";
            this.columnHeader2.Width = 80;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Số chứng từ";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Loại chứng từ";
            this.columnHeader4.Width = 120;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Đã giao KTTN";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Ghi chú";
            this.columnHeader5.Width = 270;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(55, 208);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 22;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(163, 208);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 23;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(274, 208);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 24;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.Location = new System.Drawing.Point(163, 51);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(9, 13);
            this.lblCompanyName.TabIndex = 3;
            this.lblCompanyName.Text = "|";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(457, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Đã bàn giao cho KTTN";
            // 
            // chkSendToKttn
            // 
            this.chkSendToKttn.AutoSize = true;
            this.chkSendToKttn.Location = new System.Drawing.Point(580, 70);
            this.chkSendToKttn.Name = "chkSendToKttn";
            this.chkSendToKttn.Size = new System.Drawing.Size(15, 14);
            this.chkSendToKttn.TabIndex = 27;
            this.chkSendToKttn.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(457, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Ghi chú";
            // 
            // txtNotes
            // 
            this.txtNotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNotes.Location = new System.Drawing.Point(460, 125);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(353, 106);
            this.txtNotes.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(52, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Tên đơn vị";
            // 
            // lblLastRemind
            // 
            this.lblLastRemind.AutoSize = true;
            this.lblLastRemind.Location = new System.Drawing.Point(163, 101);
            this.lblLastRemind.Name = "lblLastRemind";
            this.lblLastRemind.Size = new System.Drawing.Size(9, 13);
            this.lblLastRemind.TabIndex = 3;
            this.lblLastRemind.Text = "|";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(52, 101);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Biên bản gần đây";
            // 
            // FrmUpdateRemindDebt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 587);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblLastRemind);
            this.Controls.Add(this.chkSendToKttn);
            this.Controls.Add(this.lblCompanyName);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lvwDebtReminds);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtNotes);
            this.Controls.Add(this.txtPaperNumber);
            this.Controls.Add(this.dtPaperDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtOfficer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboPaperType);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCompany);
            this.Controls.Add(this.label1);
            this.Name = "FrmUpdateRemindDebt";
            this.Text = "Cập nhật đôn đốc nợ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOfficer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtPaperDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPaperNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboPaperType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ListView lvwDebtReminds;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chkSendToKttn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label lblLastRemind;
        private System.Windows.Forms.Label label11;
    }
}