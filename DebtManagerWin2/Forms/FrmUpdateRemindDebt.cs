﻿using DebtManagerWin.DataServices;
using DebtManagerWin.Forms;
using DebtManagerWin.Models;
using DebtManagerWin.Services;
using DebtManagerWin.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin
{
    /// <summary>
    /// Cập nhật đôn đốc nợ
    /// </summary>
    public partial class FrmUpdateRemindDebt : Form
    {
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(Program));
        private DebtDbContext db => Program.db;

        public Company CurrentCompany { get; set; }

        public FrmUpdateRemindDebt()
        {
            // ==================================================================
            // NOTE: 
            //      * lvwDebtReminds should be considered root of events chain. Changes to the list selection should be strictly controlled.
            //        Ie: special care on deselecting list
            // ==================================================================

            InitializeComponent();

            try
            {
                lvwDebtReminds.DrawColumnHeader += LvwDebtReminds_DrawColumnHeader;

                // Paper Type
                cboPaperType.ValueMember = nameof(PaperType.Id);
                cboPaperType.DisplayMember = nameof(PaperType.DisplayName);
                cboPaperType.Items.AddRange(db.PaperTypes.Where(t => t.PaperGroup == PaperGroup.DebtReminding).ToArray());

                LoadRemindDebtList();
            }
            catch (Exception ex)
            {
                _Logger.Error($"{nameof(FrmUpdateRemindDebt)} - Exception: {ex}");
            }
        }

        private void LvwDebtReminds_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.GreenYellow, e.Bounds);
            e.DrawText();
        }

        void LoadRemindDebtList()
        {
            lvwDebtReminds.Items.Clear();

            var lvItems = new List<ListViewItem>();
            db.DebtReminds.OrderByDescending(d => d.PaperDate).Include(d => d.Company).Take(AppConstants.RowsToDisplay).ToList().ForEach(item =>
            {
                var lvItem = new ListViewItem
                {
                    Text = item.Company.Name,
                    Tag = item
                };
                lvItem.SubItems.AddRange(new string[] {
                    item.PaperDate.ToString(AppConstants.DateFormat),
                    item.PaperNumber,
                    item.PaperType.Name,
                    item.SendToKttn ? "Có" : "",
                    item.Notes
                });
                lvItems.Add(lvItem);
            });

            lvwDebtReminds.Items.AddRange(lvItems.ToArray());
        }

        private void ShowSelectedBadDebt()
        {
            var debtRemind = lvwDebtReminds.SelectedItems.Count <= 0 ? null : lvwDebtReminds.SelectedItems[0].Tag as DebtRemind;

            if (debtRemind == null)
            {
                dtPaperDate.Value = AppConstants.MinDate;
                txtPaperNumber.Text = "";
                cboPaperType.SelectedItem = null;
                chkSendToKttn.Checked = false;
                txtNotes.Text = "";
                return;
            }

            dtPaperDate.Value = debtRemind.PaperDate;
            txtPaperNumber.Text = debtRemind.PaperNumber;
            cboPaperType.SelectedItem = debtRemind.PaperType;
            chkSendToKttn.Checked = debtRemind.SendToKttn;
            txtNotes.Text = debtRemind.Notes;
        }

        /// <summary>
        /// Called when entered company code changed
        /// </summary>
        private void UpdateCompanyDependentFields(bool updateList = false)
        {
            try
            {
                // Clear current debt
                if (updateList && _TriggerDebtListUpdate && lvwDebtReminds.SelectedItems.Count > 0)
                {
                    lvwDebtReminds.SelectedItems.Clear();
                }

                if (CurrentCompany == null)
                {
                    lblCompanyName.Text = "";
                    txtOfficer.Text = "";
                    lblLastRemind.Text = "";
                    return;
                }

                lblCompanyName.Text = CurrentCompany.Name;
                txtOfficer.Text = CurrentCompany.Officer?.Name;
                // Show last reminds
                var remind = CurrentCompany.DebtReminds.OrderByDescending(d => d.PaperDate).LastOrDefault();
                if (remind?.IsOverridden == false)
                {
                    lblLastRemind.Text = remind.PaperType.DisplayName;
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"{nameof(UpdateCompanyDependentFields)} - Exception: {ex}");

                MessageBox.Show($"Lỗi: {ex.Message}");
            }
        }

        private void txtCompany_Leave(object sender, EventArgs e)
        {
            // Update dependent field when company field lost focus
            if (string.IsNullOrWhiteSpace(txtCompany.Text))
                CurrentCompany = null;
            else
                CurrentCompany = db.Companies.FirstOrDefault(c => c.Code == txtCompany.Text);

            UpdateCompanyDependentFields(false);
        }

        private List<string> ValidateInputs()
        {
            var errors = new List<string>();

            if (string.IsNullOrWhiteSpace(txtCompany.Text))
            {
                errors.Add("Bạn hãy nhập mã công ty");
            }
            else
            {
                if (dtPaperDate.Value < AppConstants.ValidDate || dtPaperDate.Value > DateTime.Now)
                    errors.Add("Ngày chứng từ không hợp lệ");

                if (string.IsNullOrWhiteSpace(txtPaperNumber.Text))
                    errors.Add("Số chứng từ không hợp lệ");

                if (cboPaperType.SelectedIndex < 0)
                    errors.Add("Bạn hãy chọn loại chứng từ");
            }

            return errors;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Clear selected item
            lvwDebtReminds.SelectedIndices.Clear();
        }

        private void SaveChanges(DebtRemind debtRemind)
        {
            var errors = ValidateInputs();
            if (errors.Any())
            {
                MessageBox.Show(string.Join("\r\n", errors), "Lỗi");
                return;
            }

            var now = DateTime.Now;

            if (debtRemind == null)
            {
                debtRemind = new DebtRemind
                {
                    CreatedTime = now,
                    UpdateTime = now,
                    CompanyId = (int)CurrentCompany.Id,
                    OfficerId = CurrentCompany.Officer?.Id,
                    PaperDate = dtPaperDate.Value,
                    PaperNumber = txtPaperNumber.Text,
                    PaperTypeId = (int)(cboPaperType.SelectedItem as PaperType).Id,
                    SendToKttn = chkSendToKttn.Checked,
                    Notes = txtPaperNumber.Text,
                };

                db.DebtReminds.Add(debtRemind);
            }
            else
            {
                //debtRemind.CreatedTime = DateTime.Now;
                debtRemind.UpdateTime = DateTime.Now;
                //debtRemind.CompanyId = (int)CurrentCompany.Id;
                //debtRemind.OfficerId = CurrentCompany.Officer?.Id ?? 0;
                debtRemind.PaperDate = dtPaperDate.Value;
                debtRemind.PaperNumber = txtPaperNumber.Text;
                debtRemind.PaperTypeId = (int)(cboPaperType.SelectedItem as PaperType).Id;
                debtRemind.SendToKttn = chkSendToKttn.Checked;
                debtRemind.Notes = txtPaperNumber.Text;
            }

            db.SaveChanges();

            // Reload bad-debt list
            LoadRemindDebtList();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //if (lvwDebtReminds.SelectedItems.Count <= 0)
            //{
            //    MessageBox.Show("Bạn hãy chọn 1 dòng từ danh sách bên dưới để cập nhật");
            //    return;
            //}

            SaveChanges(lvwDebtReminds.SelectedIndices.Count > 0 ? lvwDebtReminds.SelectedItems[0].Tag as DebtRemind : null);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lvwDebtReminds.SelectedItems.Count <= 0)
                return;

            var debtRemind = lvwDebtReminds.SelectedItems[0].Tag as DebtRemind;
            db.DebtReminds.Remove(debtRemind);
            db.SaveChanges();

            // Reload bad-debt list
            LoadRemindDebtList();
        }
        private bool _TriggerDebtListUpdate = true;

        private void lvwDebtReminds_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Prevent Trigger company update
                _TriggerDebtListUpdate = false;

                if (lvwDebtReminds.SelectedItems.Count <= 0)
                {
                    txtCompany.Text = "";
                }
                else
                {
                    var badDebt = (DebtRemind)lvwDebtReminds.SelectedItems[0].Tag;
                    txtCompany.Text = badDebt.Company.Code;
                }

                txtCompany_Leave(null, null);
            }
            catch (Exception ex)
            {
                _Logger.Error($"{nameof(lvwDebtReminds_SelectedIndexChanged)} - Error: {ex}");
            }
            finally
            {
                _TriggerDebtListUpdate = true;
            }

            ShowSelectedBadDebt();
        }

        private void txtCompany_TextChanged(object sender, EventArgs e)
        {
            // reset current company
            if (CurrentCompany != null)
            {
                CurrentCompany = null;
                UpdateCompanyDependentFields(true);
            }
        }
    }
}
