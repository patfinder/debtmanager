﻿using DebtManagerWin.DataServices;
using DebtManagerWin.Models;
using DebtManagerWin.Services;
using DebtManagerWin.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace DebtManagerWin.Forms
{
    /// <summary>
    /// Xem danh sách nợ gần đây
    /// </summary>
    public partial class FrmViewDebt : Form
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));
        private DebtDbContext db => Program.db;
        private AppConfigReader configReader;

        public int PageIndex { get; set; }
        
        public int PageCount { get; set; }

        public int PageSize => 30;

        public FrmViewDebt()
        {
            configReader = new AppConfigReader();

            InitializeComponent();

            cboMonths.Items.AddRange(TimeUtils.GenerateMonthsList(AppConstants.MonthsToShow).ToArray());
            cboMonths.ValueMember = nameof(DateListItem.Value);
            cboMonths.DisplayMember = nameof(DateListItem.Display);
            cboMonths.SelectedIndex = -1;
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (PageIndex > 0)
            {
                PageIndex--;
                LoadDebtData();
            }

            CheckEnableNextPrevButtons();
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            if (PageIndex < PageCount - 1)
            {
                PageIndex++;
                LoadDebtData();
            }

            CheckEnableNextPrevButtons();
        }

        private void LoadDebtData()
        {
            try
            {
                lvwDebts.Items.Clear();
                lvwDebts.Groups.Clear();

                var month = ((DateListItem)cboMonths.SelectedItem).Value;

                var debtGroups = db.Debts
                    .Where(d => d.ImportMonth == month)
                    .OrderBy(d => d.CompanyName).Skip(PageIndex * PageSize).Take(PageSize)
                    .GroupBy(d => d.ImportMonth).ToList();

                //Debug.WriteLine($"Count: {debtGroups.Count} - key {debtGroups.FirstOrDefault()?.Key}");

                debtGroups.ForEach(g =>
                {
                    var lvGroup = new ListViewGroup(g.Key.ToString("MM/yyyy"));
                    lvwDebts.Items.AddRange(g.Select(
                        d =>
                        {
                            var item = new ListViewItem(
                                new[] { d.CompanyName, d.CompanyCode, d.tongBsg.ToString(), d.MonthsOfDebt.ToString() }
                            );
                            item.Group = lvGroup;
                            return item;
                        }).ToArray());

                    lvwDebts.Groups.Add(lvGroup);
                });
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(LoadDebtData)} - Exception: ", ex);
                MessageBox.Show("Lỗi: " + ex.Message);
            }
        }

        private void cboMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboMonths.SelectedIndex < 0)
                return;

            var rowCount = db.Debts.Where(d => d.ImportMonth == ((DateListItem)cboMonths.SelectedItem).Value).Count();
            PageCount = (rowCount + PageSize - 1) / PageSize;
            PageIndex = 0;

            CheckEnableNextPrevButtons();

            LoadDebtData();
        }

        private void CheckEnableNextPrevButtons()
        {
            btnLeft.Enabled = PageIndex > 0;
            btnRight.Enabled = PageIndex < PageCount - 1;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (cboMonths.SelectedIndex < 0)
                return;

            if (MessageBox.Show("Bạn có thực sự muốn xóa dữ liệu nợ không?", "Xác nhận", MessageBoxButtons.YesNoCancel) != DialogResult.Yes)
                return;

            try
            {
                var month = ((DateListItem)cboMonths.SelectedItem).Value;
                //db.Debts.RemoveRange(db.Debts.Where(d => d.ImportMonth == month));

                // TODO: DB dependent
                //db.Database.ExecuteSqlCommand("DELETE FROM Debt WHERE ImportMonth = @month", new SQLiteParameter("month", month));
                db.Database.ExecuteSqlCommand("DELETE FROM Debt WHERE ImportMonth = @month", new SqlParameter("month", month));

                configReader.RemoveImportedMonth(month);
                db.SaveChanges();

                MessageBox.Show("Xóa thành công");

                // Emulate selecting a month
                cboMonths_SelectedIndexChanged(null, null);
            }
            catch(Exception ex)
            {
                _logger.Error($"{nameof(btnDelete_Click)} - Exception: ", ex);
                MessageBox.Show("Lỗi: " + ex.Message);
            }
        }
    }
}
