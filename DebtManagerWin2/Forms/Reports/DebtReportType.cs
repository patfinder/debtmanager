﻿namespace DebtManagerWin.Forms.Reports
{
    public enum DebtReportType
    {
        Unknown,
        /// <summary>
        /// Báo cáo Nợ khó thu
        /// </summary>
        BadDebtReport,
        /// <summary>
        /// Báo cáo Đôn đốc nợ
        /// </summary>
        DebtRemindReport,
    }
}
