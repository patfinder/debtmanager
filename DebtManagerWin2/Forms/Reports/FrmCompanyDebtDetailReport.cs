﻿using DebtManagerWin.DataServices;
using DebtManagerWin.Forms.Reports;
using DebtManagerWin.Models;
using DebtManagerWin.Services;
using DebtManagerWin.Utils;
using log4net;
using SmartDev.Libs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin.Forms.Reports
{
    /// <summary>
    /// Báo cáo Quá trình xử lý nợ của đơn vị
    /// </summary>
    public partial class FrmCompanyDebtDetailReport : Form
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));
        private DebtDbContext db => Program.db;

        private Company company;

        public FrmCompanyDebtDetailReport()
        {
            InitializeComponent();

            cboFromMonth.Items.AddRange(Utils.TimeUtils.GenerateMonthsList(AppConstants.MonthsToShow).ToArray());
            cboFromMonth.ValueMember = nameof(DateListItem.Value);
            cboFromMonth.DisplayMember = nameof(DateListItem.Display);
            cboFromMonth.SelectedIndex = 0;

            cboToMonth.Items.AddRange(Utils.TimeUtils.GenerateMonthsList(AppConstants.MonthsToShow).ToArray());
            cboToMonth.ValueMember = nameof(DateListItem.Value);
            cboToMonth.DisplayMember = nameof(DateListItem.Display);
            cboToMonth.SelectedIndex = 0;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                if(company == null)
                {
                    MessageBox.Show("Bạn phải nhập mã công ty hợp lệ");
                    return;
                }

                var startMonth = ((DateListItem)cboFromMonth.SelectedItem).Value;
                var endMonth = ((DateListItem)cboToMonth.SelectedItem).Value;
                if (endMonth < startMonth)
                {
                    MessageBox.Show("Bạn phải chọn tháng kết thúc lớn hơn hoặc bằng tháng bắt đầu");
                    return;
                }

                if(startMonth.AddMonths(AppConstants.MaxNoOfReportedMonths) < endMonth)
                {
                    MessageBox.Show($"Số tháng báo cáo không thể lớn hơn {AppConstants.MaxNoOfReportedMonths} tháng.");
                    return;
                }

                var baseReportName = "Bao Cao Qua Trinh Xu Ly No Chi Tiet Theo Don Vi";
                var templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", $"{baseReportName}.xlsx");

                var fileName = FileUtils.ReplaceInvalidFileNameChars($"{baseReportName} - {txtCompanyName.Text} - {cboFromMonth.Text} - {IdentityServices.NewLowGuidTimestamp()}.xlsx");
                var outputPath = Path.Combine(Path.GetTempPath(), fileName);

                if (ReportUtils.GenerateCompanyDebtDetailReport(templatePath, outputPath, (int)company.Id, ((DateListItem)cboFromMonth.SelectedItem).Value, ((DateListItem)cboToMonth.SelectedItem).Value))
                {
                    _logger.Info($"{nameof(FrmDebtReport)} - File đã được xuất ra: {outputPath}");
                    Process.Start(outputPath);
                    Close();
                }
                else
                {
                    MessageBox.Show("Lỗi sinh báo cáo.\r\nXin thử lại hoặc liên hệ trợ giúp.", "Lỗi");
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(FrmDebtReport)} - Exception: {ex}");
                MessageBox.Show($"Lỗi: {ex.Message}");
            }
        }

        private void txtCompanyCode_TextChanged(object sender, EventArgs e)
        {
            company = db.Companies.FirstOrDefault(c => c.Code == txtCompanyCode.Text);


            txtCompanyName.Text = company?.Name ?? "<Mã công ty không tồn tại>";
        }
    }
}
