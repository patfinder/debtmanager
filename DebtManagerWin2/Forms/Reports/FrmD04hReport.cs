﻿using DebtManagerWin.DataServices;
using DebtManagerWin.Models;
using DebtManagerWin.ReportModels;
using DebtManagerWin.Services;
using DebtManagerWin.Utils;
using log4net;
using Newtonsoft.Json;
using SmartDev.Libs;
using SoftArtisans.OfficeWriter.WordWriter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin.Forms.Reports
{
    /// <summary>
    /// Xuất văn bản D04H
    /// </summary>
    public partial class FrmD04hReport : Form
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));
        private DebtDbContext db => Program.db;

        public Company CurrentCompany { get; set; }

        public FrmD04hReport()
        {
            InitializeComponent();

            cboPaperMonth.Items.AddRange(Utils.TimeUtils.GenerateMonthsList(AppConstants.MonthsToShow).ToArray());
            cboPaperMonth.ValueMember = nameof(DateListItem.Value);
            cboPaperMonth.DisplayMember = nameof(DateListItem.Display);
            cboPaperMonth.SelectedIndex = 0;

            cboDeadline.Items.AddRange(Utils.TimeUtils.GenerateMonthsListForward(AppConstants.MonthsToShow).ToArray());
            cboDeadline.ValueMember = nameof(DateListItem.Value);
            cboDeadline.DisplayMember = nameof(DateListItem.Display);
            cboDeadline.SelectedIndex = 0;
        }

        private void txtCompanyCode_Leave(object sender, EventArgs e)
        {
            // Code changed
            if(CurrentCompany == null || CurrentCompany.Code != txtCompanyCode.Text || string.IsNullOrWhiteSpace(txtCompanyCode.Text))
            {
                if (string.IsNullOrWhiteSpace(txtCompanyCode.Text))
                    CurrentCompany = null;
                else
                    CurrentCompany = db.Companies.FirstOrDefault(c => c.Code.ToLower() == txtCompanyCode.Text.ToLower());

                // Update company name
                txtCompanyName.Text = CurrentCompany?.Name;

                // Debt info
                UpdateDebtInfo();
            }
        }

        private void UpdateDebtInfo()
        {
            var prevMonth = ((DateListItem)cboPaperMonth.SelectedItem).Value.AddMonths(-1);
            Debt debt = CurrentCompany?.Debts?.FirstOrDefault(d => d.ImportMonth == prevMonth);
            if (debt != null) // tongBhCk
                txtDebtInfo.Text = $"Tháng nợ: {debt.ImportMonth.ToString("MM/yyyy")} - Số tiền nợ: {debt._tien_ck}";
            else
                txtDebtInfo.Text = "<không tồn tại>";
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (CurrentCompany == null)
                {
                    MessageBox.Show("Bạn hãy nhập mã công ty chính xác");
                    return;
                }

                var errors = ValidateInputs();
                if(errors.Any())
                {
                    MessageBox.Show(string.Join("\r\n", errors), "Lỗi");
                    return;
                }

                // selected month debt
                var paperMonth = ((DateListItem)cboPaperMonth.SelectedItem).Value;
                var prevMonth = paperMonth.AddMonths(-1);
                Debt debt = CurrentCompany?.Debts?.FirstOrDefault(d => d.ImportMonth == prevMonth);
                if (debt == null)
                {
                    MessageBox.Show("Tháng báo cáo bạn chọn không có dữ liệu");
                    return;
                }

                var lastDayOfMonth = new DateTime(paperMonth.Year, paperMonth.Month, DateTime.DaysInMonth(paperMonth.Year, paperMonth.Month));

                var reportModel = new D04hReportModel
                {
                    PaperMonth = paperMonth.ToString(AppConstants.MonthFormat),
                    CompanyName = txtCompanyName.Text,
                    NoOfEmployee = $"{CurrentCompany.NoOfEmployees:D2}",
                    Leader = $"{txtLeaderName.Text}",
                    LeaderTitle = $"{txtResponsibility.Text}",
                    Accountable = CurrentCompany.Officer?.Name,
                    MonthEnd = lastDayOfMonth.ToString(AppConstants.DateFormat),
                    DebtTotal = $"{debt._tien_ck}", // tongBhCk
                    TotalSalaryFund = $"{debt.TotalSalaryFund}",
                    PayDeadline = ((DateListItem)cboDeadline.SelectedItem).Value.ToString(AppConstants.DateFormat),
                };

                var fileName = FileUtils.ReplaceInvalidFileNameChars($"{txtCompanyName.Text}-{cboPaperMonth.Text}-{IdentityServices.NewLowGuidTimestamp()}.doc");
                string outputPath = Path.Combine(Path.GetTempPath(), fileName);

                if (ReportUtils.GenerateD04hDocument(reportModel, outputPath))
                {
                    _logger.Info($"{nameof(FrmD04hReport)} - File đã được xuất ra: {outputPath}");
                    Process.Start(outputPath);
                    Close();
                }
                else
                {
                    MessageBox.Show("Lỗi sinh báo cáo.\r\nXin thử lại hoặc liên hệ trợ giúp.", "Lỗi");
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(FrmD04hReport)} - Exception: {ex}");
                MessageBox.Show($"Lỗi: {ex.Message}");
            }
        }

        private List<string> ValidateInputs()
        {
            var errors = new List<string>();

            if (cboPaperMonth.SelectedIndex < 0)
            {
                errors.Add("Bạn hãy chọn tháng báo cáo.");
            }
            
            return errors;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cboPaperMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDebtInfo();
        }
    }
}
