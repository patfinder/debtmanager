﻿using DebtManagerWin.Forms.Reports;
using DebtManagerWin.Services;
using DebtManagerWin.Utils;
using log4net;
using SmartDev.Libs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin.Forms.Reports
{
    /// <summary>
    /// Phát sinh báo cáo nợ khó đòi.
    /// Or "Biên bản bàn giao hồ sơ công nợ"
    /// Or "Báo cáo tình hình đôn đốc nợ"
    /// </summary>
    public partial class FrmDebtReport : Form
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));
        private DebtReportType reportType;
        private bool sendToKttn;

        public FrmDebtReport(DebtReportType reportType, bool sendToKttn = false)
        {
            InitializeComponent();

            this.reportType = reportType;
            this.sendToKttn = sendToKttn;

            this.Text = "Phát sinh " + (reportType == DebtReportType.BadDebtReport ? "Bao Cao Don Vi No Kho Thu" :
                                    sendToKttn ? "Bien Ban Ban Giao Ho So Cong No" : "Bao Cao Tinh Hinh Don Doc No");

            cboMonths.Items.AddRange(Utils.TimeUtils.GenerateMonthsList(AppConstants.MonthsToShow).ToArray());
            cboMonths.ValueMember = nameof(DateListItem.Value);
            cboMonths.DisplayMember = nameof(DateListItem.Display);
            cboMonths.SelectedIndex = 0;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                var baseReportName = reportType == DebtReportType.BadDebtReport ? "Bao Cao Don Vi No Kho Thu" :
                    sendToKttn ? "Bien Ban Ban Giao Ho So Cong No" : "Bao Cao Tinh Hinh Don Doc No";

                var templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", $"{baseReportName}.xlsx");

                var fileName = FileUtils.ReplaceInvalidFileNameChars($"{baseReportName} - {cboMonths.Text} - {IdentityServices.NewLowGuidTimestamp()}.xlsx");
                var outputPath = Path.Combine(Path.GetTempPath(), fileName);

                var result = reportType == DebtReportType.BadDebtReport ?
                    ReportUtils.GenerateBadDebtReport(templatePath, outputPath, ((DateListItem)cboMonths.SelectedItem).Value) :
                    ReportUtils.GenerateDebtRemindReport(sendToKttn, templatePath, outputPath, ((DateListItem)cboMonths.SelectedItem).Value, DebtRemindReportType.DebtRemindStatus);

                if (result)
                {
                    _logger.Info($"{nameof(FrmDebtReport)} - File đã được xuất ra: {outputPath}");
                    Process.Start(outputPath);
                    Close();
                }
                else
                {
                    MessageBox.Show("Lỗi phát sinh báo cáo.\r\nXin thử lại hoặc liên hệ trợ giúp.", "Lỗi");
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(FrmDebtReport)} - Exception: {ex}");
                MessageBox.Show($"Lỗi: {ex.Message}");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
