﻿namespace DebtManagerWin.Migrations
{
    using DebtManagerWin.DataServices;
    using DebtManagerWin.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DebtDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DebtDbContext context)
        {
            try
            {
                // Only called when run "Update-Database" EF command

                //  This method will be called after migrating to the latest version.

                //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
                //  to avoid creating duplicate seed data.

                var paperTypes = new PaperType[]
                {
                    new PaperType{Id = 1, PaperGroup = PaperGroup.CompanyNotification, Name = "Công văn"},
                    new PaperType{Id = 2, PaperGroup = PaperGroup.CompanyNotification, Name = "Quyết định"},
                };
                context.PaperTypes.AddOrUpdate(a => a.Name, paperTypes);
                context.SaveChanges();

                paperTypes = new PaperType[]
                {
                    new PaperType{Id = 3, PaperGroup = PaperGroup.DebtReminding, Name = "Biên bản làm việc D04h-TS"},
                    new PaperType{Id = 4, PaperGroup = PaperGroup.DebtReminding, Name = "Thông báo nợ lần 1"},
                    new PaperType{Id = 5, PaperGroup = PaperGroup.DebtReminding, Name = "Thông báo nợ lần 2"},
                    new PaperType{Id = 6, PaperGroup = PaperGroup.DebtReminding, Name = "Thông báo kiểm tra đơn vị"},
                    //new PaperType{Id = 7, PaperGroup = PaperGroup.DebtReminding, Name = "Biên bản kiểm tra đơn vị"},
                };
                context.PaperTypes.AddOrUpdate(a => a.Name, paperTypes);
                context.SaveChanges();

                var badDebtTypes = new BadDebtType[]
                {
                    new BadDebtType{Id = 1, Name = "Mất tích",      Description = "Đơn vị không còn tại địa điểm đăng ký kinh doanh"},
                    new BadDebtType{Id = 2, Name = "Chờ giải thể",  Description = "Đơn vị đang trong thời gian làm thủ tục giải thể, phá sản; đơn vị có chủ là người nước ngoài bỏ trốn khỏi Việt Nam; đơn vị không hoạt động, không có người quản lý, điều hành."},
                    new BadDebtType{Id = 3, Name = "Hết hoạt động", Description = "Đơn vị chấm dứt hoạt động, giải thể, phá sản theo quy định của pháp luật"},
                    new BadDebtType{Id = 4, Name = "Nợ khác",       Description = "Đơn vị nợ đang trong thời gian được tạm dừng đóng vào quỹ hưu trí và từ tuất"},
                };
                context.BadDebtTypes.AddOrUpdate(a => a.Name, badDebtTypes);
                context.SaveChanges();

                var departments = new Department[]
                {
                    new Department{Id = 1, Name = "UBND"},
                    new Department{Id = 2, Name = "Thuế"},
                    new Department{Id = 3, Name = "Công an"},
                    new Department{Id = 4, Name = "Sở KH ĐT"},
                    new Department{Id = 5, Name = "Tòa án"},
                    new Department{Id = 6, Name = "BHXH"},
                };
                context.Departments.AddOrUpdate(a => a.Name, departments);
                context.SaveChanges();

                var officers = new Officer[]
                {
                    new Officer{Id = 1 , Name = "buihung",    FullName = "buihung",      StaffId = "buihung"},
                    new Officer{Id = 2 , Name = "dotuyet",    FullName = "dotuyet",      StaffId = "dotuyet"},
                    new Officer{Id = 3 , Name = "dung_sms",   FullName = "dung_sms",     StaffId = "dung_sms"},
                    new Officer{Id = 4 , Name = "hoanglien",  FullName = "hoanglien",    StaffId = "hoanglien"},
                    new Officer{Id = 5 , Name = "huyentrang", FullName = "huyentrang",   StaffId = "huyentrang"},
                    new Officer{Id = 6 , Name = "kimnb",      FullName = "kimnb",        StaffId = "kimnb"},
                    new Officer{Id = 7 , Name = "minhduc",    FullName = "minhduc",      StaffId = "minhduc"},
                    new Officer{Id = 8 , Name = "myle",       FullName = "myle",         StaffId = "myle"},
                    new Officer{Id = 9 , Name = "ngoctram",   FullName = "ngoctram",     StaffId = "ngoctram"},
                    new Officer{Id = 10, Name = "nhi_sms",    FullName = "nhi_sms",      StaffId = "nhi_sms"},
                    new Officer{Id = 11, Name = "quynhlan",   FullName = "quynhlan",     StaffId = "quynhlan"},
                    new Officer{Id = 12, Name = "thanh_ttd",  FullName = "thanh_ttd",    StaffId = "thanh_ttd"},
                    new Officer{Id = 13, Name = "thuydung",   FullName = "thuydung",     StaffId = "thuydung"},
                    new Officer{Id = 14, Name = "xuanhai",    FullName = "xuanhai",      StaffId = "xuanhai"},
                    new Officer{Id = 15, Name = "xuanvan",    FullName = "xuanvan",      StaffId = "xuanvan"},
                };
                context.Officers.AddOrUpdate(a => a.Name, officers);
                context.SaveChanges();

                var now = DateTime.Now;
                var companies = new Company[]
                {
                    new Company{Id = 1 , Code = "AA0001Z", OfficerId = 1 , LastDebtUpdate = now, Name = "Đối tượng chính sách (TTĐD & CS người có công)" },
                    new Company{Id = 2 , Code = "AA0002Z", OfficerId = 2 , LastDebtUpdate = now, Name = "Đối tượng BTXH (TT Bảo trợ xã hội tỉnh)" },
                    new Company{Id = 3 , Code = "AD0001Z", OfficerId = 3 , LastDebtUpdate = now, Name = "Người lao động hưởng chế độ ốm đau" },
                    new Company{Id = 4 , Code = "AG0001Z", OfficerId = 4 , LastDebtUpdate = now, Name = "CBXP Nha Trang (Hưởng TC từ BHXH)" },
                    new Company{Id = 5 , Code = "AG0002Z", OfficerId = 5 , LastDebtUpdate = now, Name = "CBXP Vạn Ninh (Hưởng TC từ BHXH)" },
                    new Company{Id = 6 , Code = "AG0003Z", OfficerId = 6 , LastDebtUpdate = now, Name = "CBXP  Thị xã Ninh Hòa (Hưởng TC từ BHXH)" },
                    new Company{Id = 7 , Code = "AG0004Z", OfficerId = 7 , LastDebtUpdate = now, Name = "CBXP Diên Khánh (Hưởng TC từ BHXH)" },
                    new Company{Id = 8 , Code = "AG0005Z", OfficerId = 8 , LastDebtUpdate = now, Name = "CBXP Cam Ranh (Hưởng TC từ BHXH)" },
                    new Company{Id = 9 , Code = "AG0006Z", OfficerId = 9 , LastDebtUpdate = now, Name = "CBXP Khánh Vĩnh (Hưởng TC từ BHXH)" },
                    new Company{Id = 10, Code = "AG0007Z", OfficerId = 10, LastDebtUpdate = now, Name = "CBXP Khánh Sơn (Hưởng TC từ BHXH)" },
                    new Company{Id = 11, Code = "AG0008Z", OfficerId = 11, LastDebtUpdate = now, Name = "CBXP Cam Lâm (Hưởng TC từ BHXH)" },
                    new Company{Id = 12, Code = "AL0002Z", OfficerId = 12, LastDebtUpdate = now, Name = "Trường Đại Học Thái Bình Dương" },
                    new Company{Id = 13, Code = "AL0003Z", OfficerId = 13, LastDebtUpdate = now, Name = "Trường Dự bị đại học dân tộc trung ương nha trang - HS" },
                    new Company{Id = 14, Code = "AL0004Z", OfficerId = 14, LastDebtUpdate = now, Name = "Trường Cao đẳng Nghề Du lịch Nha Trang" },
                    new Company{Id = 15, Code = "AL0005Z", OfficerId = 15, LastDebtUpdate = now, Name = "Trường Đại Học Nha Trang" },
                    new Company{Id = 16, Code = "AL0006Z", OfficerId = 1 , LastDebtUpdate = now, Name = "Trường trung cấp chuyên nghiệp Nha Trang -(ĐHTĐT)" },
                    new Company{Id = 17, Code = "AL0007Z", OfficerId = 2 , LastDebtUpdate = now, Name = "Trường Cao đẳng Kỹ thuật Công nghệ Nha Trang" },
                };

                context.Companies.AddOrUpdate(a => a.Code, companies);
                context.SaveChanges();


                var createTime = DateTime.Now;

                var eventDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1);
                var paperDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                var badDebts = new GovernmentalDocument[]
                {
                    new GovernmentalDocument{Id = 1, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 1, OfficerId = 1, PaperDate = paperDate, PaperNumber = "111", PaperTypeId = 4, DepartmentId = 2, DebtMonth = eventDate, BadDebtTypeId = 1, UnpaidEmployeeCount = 0 },
                    new GovernmentalDocument{Id = 2, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 2, OfficerId = 2, PaperDate = paperDate, PaperNumber = "222", PaperTypeId = 5, DepartmentId = 2, DebtMonth = eventDate, BadDebtTypeId = 2, UnpaidEmployeeCount = 1 },
                    new GovernmentalDocument{Id = 3, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 3, OfficerId = 3, PaperDate = paperDate, PaperNumber = "333", PaperTypeId = 4, DepartmentId = 3, DebtMonth = eventDate, BadDebtTypeId = 3, UnpaidEmployeeCount = 2 },
                    new GovernmentalDocument{Id = 4, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 4, OfficerId = 4, PaperDate = paperDate, PaperNumber = "444", PaperTypeId = 5, DepartmentId = 4, DebtMonth = eventDate, BadDebtTypeId = 4, UnpaidEmployeeCount = 4 },
                    new GovernmentalDocument{Id = 5, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 5, OfficerId = 5, PaperDate = paperDate, PaperNumber = "555", PaperTypeId = 4, DepartmentId = 2, DebtMonth = eventDate, BadDebtTypeId = 1, UnpaidEmployeeCount = 0 },
                    new GovernmentalDocument{Id = 6, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 6, OfficerId = 1, PaperDate = paperDate, PaperNumber = "666", PaperTypeId = 5, DepartmentId = 2, DebtMonth = eventDate, BadDebtTypeId = 2, UnpaidEmployeeCount = 1 },
                    new GovernmentalDocument{Id = 7, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 7, OfficerId = 2, PaperDate = paperDate, PaperNumber = "777", PaperTypeId = 4, DepartmentId = 3, DebtMonth = eventDate, BadDebtTypeId = 3, UnpaidEmployeeCount = 2 },
                    new GovernmentalDocument{Id = 8, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 8, OfficerId = 3, PaperDate = paperDate, PaperNumber = "888", PaperTypeId = 5, DepartmentId = 4, DebtMonth = eventDate, BadDebtTypeId = 4, UnpaidEmployeeCount = 4 },
                    new GovernmentalDocument{Id = 9, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 9, OfficerId = 4, PaperDate = paperDate, PaperNumber = "999", PaperTypeId = 4, DepartmentId = 2, DebtMonth = eventDate, BadDebtTypeId = 1, UnpaidEmployeeCount = 0 },
                    new GovernmentalDocument{Id = 10, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 10, OfficerId = 5, PaperDate = paperDate, PaperNumber = "000", PaperTypeId = 5, DepartmentId = 2, DebtMonth = eventDate, BadDebtTypeId = 2, UnpaidEmployeeCount = 1 },
                    new GovernmentalDocument{Id = 11, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 11, OfficerId = 1, PaperDate = paperDate, PaperNumber = "111", PaperTypeId = 4, DepartmentId = 3, DebtMonth = eventDate, BadDebtTypeId = 3, UnpaidEmployeeCount = 2 },
                    new GovernmentalDocument{Id = 12, CreatedTime = createTime, UpdateTime = createTime, CompanyId = 12, OfficerId = 2, PaperDate = paperDate, PaperNumber = "222", PaperTypeId = 5, DepartmentId = 4, DebtMonth = eventDate, BadDebtTypeId = 4, UnpaidEmployeeCount = 4 },
                };
                context.GovernmentalDocuments.AddOrUpdate(a => new { a.CompanyId, a.DebtMonth }, badDebts);
                context.SaveChanges();

                var debtReminds = new DebtRemind[]
                {
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 1, IsOverridden = true,  OfficerId = 4, PaperDate= paperDate, PaperNumber = "CT 0001", PaperTypeId = 1, SendToKttn = false, Notes = "Notes AAA 01", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 2, IsOverridden = false, OfficerId = 3, PaperDate= paperDate, PaperNumber = "CT 0002", PaperTypeId = 2, SendToKttn = false, Notes = "Notes BBB 02", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 3, IsOverridden = true,  OfficerId = 4, PaperDate= paperDate, PaperNumber = "CT 0003", PaperTypeId = 1, SendToKttn = true,  Notes = "Notes AAA 03", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 4, IsOverridden = false, OfficerId = 3, PaperDate= paperDate, PaperNumber = "CT 0004", PaperTypeId = 2, SendToKttn = false, Notes = "Notes BBB 04", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 5, IsOverridden = true,  OfficerId = 4, PaperDate= paperDate, PaperNumber = "CT 0005", PaperTypeId = 1, SendToKttn = false, Notes = "Notes AAA 05", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 6, IsOverridden = false, OfficerId = 3, PaperDate= paperDate, PaperNumber = "CT 0006", PaperTypeId = 2, SendToKttn = false, Notes = "Notes BBB 06", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 1, IsOverridden = true,  OfficerId = 4, PaperDate= paperDate, PaperNumber = "CT 0007", PaperTypeId = 1, SendToKttn = true,  Notes = "Notes AAA 07", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 2, IsOverridden = false, OfficerId = 3, PaperDate= paperDate, PaperNumber = "CT 0008", PaperTypeId = 2, SendToKttn = true,  Notes = "Notes BBB 08", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 3, IsOverridden = true,  OfficerId = 4, PaperDate= paperDate, PaperNumber = "CT 0009", PaperTypeId = 1, SendToKttn = true,  Notes = "Notes AAA 09", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 4, IsOverridden = false, OfficerId = 3, PaperDate= paperDate, PaperNumber = "CT 0010", PaperTypeId = 2, SendToKttn = true,  Notes = "Notes BBB 10", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 1, IsOverridden = true,  OfficerId = 4, PaperDate= paperDate, PaperNumber = "CT 0011", PaperTypeId = 1, SendToKttn = false, Notes = "Notes AAA 11", },
                    new DebtRemind{CreatedTime = createTime, UpdateTime = createTime, CompanyId = 2, IsOverridden = false, OfficerId = 3, PaperDate= paperDate, PaperNumber = "CT 0012", PaperTypeId = 2, SendToKttn = false, Notes = "Notes BBB 12", }
                };
                context.DebtReminds.AddOrUpdate(a => new { a.CompanyId, a.PaperNumber }, debtReminds);
                context.SaveChanges();
                //var d = DateTime.Parse()
            }
            catch (Exception ex)
            {
                var ex2 = ex as DbEntityValidationException ?? new DbEntityValidationException();

                var errors = new List<string>();
                ex2.EntityValidationErrors.ToList().ForEach(e =>
                {
                    e.ValidationErrors.ToList().ForEach(v =>
                    {
                        errors.Add($"{e.Entry.Entity.GetType().FullName} - {v.PropertyName} - {v.ErrorMessage}");
                    });
                });

                var valErrors = string.Join("\n----------------------", errors);

                File.WriteAllText(@"d:\tmp\log1.txt", $"Exception: {ex2.Message}\nEntityValidationErrors:\n{valErrors}");
                Debug.WriteLine($"Exception: {ex}");
            }
        }
    }
}
