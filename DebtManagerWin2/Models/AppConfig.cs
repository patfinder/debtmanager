﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using System.ComponentModel.DataAnnotations;

namespace DebtManagerWin.Models
{
    public class AppConfig // : EntityBase
    {
        public int? Id { get; set; }

        // Id will be mapped to AppConfigField enum.
        [DisplayName("Tên")]
        [MaxLength(100)]
        [Index("IX_NameUnique", 1, IsUnique = true)]
        public string Name { get; set; }

        virtual public string DisplayName => Name + " - " + Id;

        public string Type { get; set; }

        public string Value { get; set; }
    }
}
