﻿using DebtManagerWin.DataServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    public enum AppConfigField
    {
        Unknown,
        [FieldType(typeof(DateTime))]
        ImportedMonths,
    }

    public class AppConfigReader
    {
        private DebtDbContext db => Program.db;

        public AppConfigReader()
        {
        }

        public DateTime[] GetImportedMonths()
        {
            // TOD_: debug
            //var l = db.AppConfigs.ToList();

            var months = db.AppConfigs.FirstOrDefault(c => c.Id == (int)AppConfigField.ImportedMonths)?.Value;
            if (string.IsNullOrEmpty(months))
                return new DateTime[0];

            return JsonConvert.DeserializeObject<DateTime[]>(months);
        }

        public void AddImportedMonth(DateTime month)
        {
            month = new DateTime(month.Year, month.Month, 1);

            var months = GetImportedMonths().ToList();
            if (months.Contains(month))
                return;

            months.Add(month);
            var monthsValue = JsonConvert.SerializeObject(months.OrderBy(m => m));
            var configName = AppConfigField.ImportedMonths.ToString();
            db.AppConfigs.AddOrUpdate(c => c.Name, new AppConfig
            {
                // TODO: Id seem to be ignored by EF
                Id = (int)AppConfigField.ImportedMonths,
                Name = configName,
                Type = typeof(DateTime[]).ToString(),
                Value = monthsValue,
            });
        }

        public void RemoveImportedMonth(DateTime month)
        {
            month = new DateTime(month.Year, month.Month, 1);

            var months = GetImportedMonths().ToList();
            if (!months.Contains(month))
                return;

            months = months.Except(new[] { month }).ToList();
            var monthsValue = JsonConvert.SerializeObject(months.OrderBy(m => m));
            var configName = AppConfigField.ImportedMonths.ToString();
            db.AppConfigs.AddOrUpdate(c => c.Name, new AppConfig
            {
                Id = (int)AppConfigField.ImportedMonths,
                Name = configName,
                Type = typeof(DateTime[]).ToString(),
                Value = monthsValue,
            });
        }
    }

    public class AppConfigImportedMonths
    {
        private DateTime[] ImportMonths { get; set; }

        internal AppConfigImportedMonths(DateTime[] importMonths)
        {
            ImportMonths = importMonths;
        }

        public DateTime this[int index]
        {
            get
            {
                return ImportMonths[index];
            }

            set
            {
                ImportMonths[index] = value;
            }
        }
    }
}
