﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    /// <summary>
    /// Các loại nợ khó thu:
    /// Đơn vị không còn tại địa điểm đăng ký kinh doanh
    /// Đơn vị đang trong thời gian làm thủ tục giải thể, phá sản; đơn vị có chủ là người nước ngoài bỏ trốn khỏi Việt Nam; đơn vị không hoạt động, không có người quản lý, điều hành.
    /// Đơn vị chấm dứt hoạt động, giải thể, phá sản theo quy định của pháp luật
    /// Đơn vị nợ đang trong thời gian được tạm dừng đóng vào quỹ hưu trí và từ tuất
    /// </summary>
    [DisplayName("Loại nợ")]
    public class BadDebtType // : CatalogBasedType
    {
        public int? Id { get; set; }

        [DisplayName("Tên")]
        [MaxLength(200)]
        public string Name { get; set; }

        virtual public string DisplayName => Name + " - " + Id;

        [DisplayName("Mô tả")]
        public string Description { get; set; }
    }
}
