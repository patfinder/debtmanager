﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    abstract public class CatalogBasedType: EntityBase
    {
        [DisplayName("Tên")]
        [MaxLength(200)]
        public string Name { get; set; }

        virtual public string DisplayName => Name + " - " + Id;
    }
}
