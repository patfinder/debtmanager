﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    [DisplayName("Công ty")]
    public class Company // : CatalogBasedType
    {
        public int? Id { get; set; }

        [DisplayName("Tên")]
        [MaxLength(200)]
        public string Name { get; set; }

        [DisplayName("Số đăng kí")]
        [MaxLength(30)]
        public string Code { get; set; }

        virtual public string DisplayName => Name + " - " + Code;

        public int? OfficerId { get; set; }

        public virtual Officer Officer { get; set; }

        public int NoOfEmployees { get; set; }

        public DateTime LastDebtUpdate { get; set; }

        public int MonthsOfDebt { get; set; }

        public virtual ICollection<Debt> Debts { get; set; }

        public virtual ICollection<GovernmentalDocument> BadDebts { get; set; }

        public virtual ICollection<DebtRemind> DebtReminds { get; set; }
    }
}
