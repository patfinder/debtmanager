﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    public class Debt // : EntityBase
    {
        public int? Id { get; set; }


        [XlsFieldName("madvi")]
        public string CompanyCode { get; set; }

        [XlsFieldName("tendvi")]
        public string CompanyName { get; set; }

        [XlsFieldName("diachi")]
        public string Address { get; set; }
        // value: dung_sms, 

        [XlsFieldName("dsnv")]
        public string Officers { get; set; }

        [XlsFieldName("_tien_dk")]
        public double _tien_dk { get; set; }

        [XlsFieldName("sld")]
        public double NoOfEmployees { get; set; }

        [XlsFieldName("sothang")]
        public double MonthsOfDebt { get; set; }

        [XlsFieldName("_tien_unc")]
        public double _tien_unc { get; set; }

        [XlsFieldName("_laiqh")]
        public double OverdueInterests { get; set; }

        [XlsFieldName("_ck_lai")]
        public double _ck_lai { get; set; }

        [XlsFieldName("_tien_ck")]
        public double _tien_ck { get; set; }

        [XlsFieldName("_tql")]
        public double TotalSalaryFund { get; set; }

        [XlsFieldName("_lai1_ps")]
        public double _lai1_ps { get; set; }

        [XlsFieldName("tongBhCk")]
        public double tongBhCk { get; set; }

        [XlsFieldName("tongBst")]
        public double tongBst { get; set; }

        [XlsFieldName("tongBsg")]
        public double tongBsg { get; set; }

        /// <summary>
        /// Month of import data. Data of each month is supposed to be imported at the end of the selected month.
        /// </summary>
        public DateTime ImportMonth { get; set; }

        public int CompanyId { get; set; }

        public virtual Company Company { get; set; }

    }
}
