﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    /// <summary>
    /// Nợ khó thu
    /// </summary>
    public class DebtRemind // : EntityBase
    {
        public int? Id { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public int CompanyId { get; set; }

        /// <summary>
        /// True if this remind is overridden by an import after that
        /// Import process will check and update this flag
        /// </summary>
        public bool IsOverridden { get; set; }

        /// <summary>
        /// Cán bộ chuyên quản
        /// </summary>
        public int? OfficerId { get; set; }

        /// <summary>
        /// NGÀY CHỨNG TỪ
        /// </summary>
        public DateTime PaperDate { get; set; }

        /// <summary>
        /// SỐ CHỨNG TỪ
        /// </summary>
        [MaxLength(30)]
        public string PaperNumber { get; set; }

        /// <summary>
        /// LOẠI CHỨNG TỪ
        /// </summary>
        public int PaperTypeId { get; set; }

        /// <summary>
        /// CƠ QUAN BAN HÀNH CHỨNG TỪ
        /// </summary>
        public int? DepartmentId { get; set; }

        /// <summary>
        /// NÚT TÍCH ĐÃ BÀN GIAO CHO PHÒNG KTTN
        /// </summary>
        public bool SendToKttn { get; set; }

        /// <summary>
        /// Ghi chú
        /// </summary>
        public string Notes { get; set; }

        public virtual Company Company { get; set; }

        public virtual Officer Officer { get; set; }

        public virtual PaperType PaperType { get; set; }

        public virtual Department Department { get; set; }
    }
}
