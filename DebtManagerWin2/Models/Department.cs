﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    [DisplayName("Cơ quan quản lí")]
    public class Department // : CatalogBasedType
    {
        public int? Id { get; set; }

        [DisplayName("Tên")]
        [MaxLength(200)]
        public string Name { get; set; }

        virtual public string DisplayName => Name + " - " + Id;
    }
}
