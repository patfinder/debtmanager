﻿using SmartDev.Libs;
using System;

namespace DebtManagerWin.Models
{
    /// <summary>
    /// Define type of DB dynamic field.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = true)]
    public class FieldTypeAttribute : Attribute, IAttribute<Type>
    {
        private Type value { get; set; }

        public FieldTypeAttribute(Type type)
        {
            value = type;
        }

        public Type Value
        {
            get => value;
        }
    }
}