﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    /// <summary>
    /// Nợ khó thu (Tên cũ BadDebt)
    /// Công văn của cơ quan quản lý về đơn vị
    /// </summary>
    public class GovernmentalDocument // : EntityBase
    {
        public int? Id { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime UpdateTime { get; set; }

        public int CompanyId { get; set; }

        /// <summary>
        /// Cán bộ chuyên quản
        /// </summary>
        public int? OfficerId { get; set; }

        /// <summary>
        /// NGÀY CHỨNG TỪ
        /// </summary>
        public DateTime PaperDate { get; set; }

        /// <summary>
        /// SỐ CHỨNG TỪ
        /// </summary>
        public string PaperNumber { get; set; }

        /// <summary>
        /// LOẠI CHỨNG TỪ
        /// </summary>
        public int PaperTypeId { get; set; }
        
        /// <summary>
        /// CƠ QUAN BAN HÀNH CHỨNG TỪ
        /// </summary>
        public int DepartmentId { get; set; }
        
        /// <summary>
        /// THÁNG XÁC ĐỊNH ĐƠN VỊ NỢ KHÓ THU
        /// </summary>
        public DateTime DebtMonth { get; set; }

        /// <summary>
        /// Số lao động chưa giải quyết chế độ tại thời điểm khoanh nợ
        /// </summary>
        public int UnpaidEmployeeCount { get; set; }

        /// <summary>
        /// Loại nợ khó thu
        /// </summary>
        public int BadDebtTypeId { get; set; }
        
        public virtual Company Company { get; set; }

        public virtual Officer Officer { get; set; }

        public virtual PaperType PaperType { get; set; }

        public virtual Department Department { get; set; }

        public virtual BadDebtType BadDebtType { get; set; }
    }
}
