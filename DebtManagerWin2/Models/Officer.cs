﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    [DisplayName("Cán bộ chuyên quản")]
    public class Officer // : CatalogBasedType
    {
        public int? Id { get; set; }

        [DisplayName("Tên")]
        [MaxLength(200)]
        public string Name { get; set; }

        [DisplayName("Mã cán bộ")]
        [MaxLength(30)]
        public string StaffId { get; set; }

        [DisplayName("Họ và Tên")]
        public string FullName { get; set; }

        public string DisplayName => Name + " - " + StaffId;

        public virtual ICollection<Company> Companies { get; set; }

        public virtual ICollection<GovernmentalDocument> GovernmentalDocuments { get; set; }

        public virtual ICollection<DebtRemind> DebtReminds { get; set; }
    }
}
