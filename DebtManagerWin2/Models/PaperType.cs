﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Models
{
    [DisplayName("Công văn")]
    public class PaperType // : CatalogBasedType
    {
        public int? Id { get; set; }

        [DisplayName("Tên")]
        [MaxLength(200)]
        public string Name { get; set; }

        virtual public string DisplayName => Name + " - " + Id;

        public PaperGroup PaperGroup { get; set; }
    }

    public enum PaperGroup
    {
        /// <summary>
        /// Thông báo về tình hình cty của các cơ quan NN
        /// </summary>
        CompanyNotification = 1,
        /// <summary>
        /// Công văn nhắc nợ
        /// </summary>
        DebtReminding
    }
}
