﻿using Autofac.Core;
using DebtManagerWin.DataServices;
using DebtManagerWin.Models;
using DebtManagerWin.Services;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DebtManagerWin
{
    static class Program
    {
        //private static Container container;
        public static DebtDbContext db = new DebtDbContext();

        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            XmlConfigurator.Configure();

            AppDomain.CurrentDomain.UnhandledException += UnhandledException_Handler;

            _logger.Info("============================================");
            _logger.Info("Application started.");

            _logger.Info($"Connection string: {db.Database.Connection.ConnectionString}");

            //Mapper.Initialize(cfg => {
            //    cfg.CreateMap<Foo, FooDto>();
            //    cfg.CreateMap<Bar, BarDto>();
            //});

            // Start remote service host.
            //RemoteServiceHost.Start();




            //TestDb();




            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Bootstrap();

            Application.Run(new FrmMain());

            _logger.Info("Application finished.");
        }

        static void UnhandledException_Handler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception ex = (Exception)args.ExceptionObject;
            _logger.Debug($"UnhandledException: {ex.Message} - StackTrace: {ex.StackTrace}");
            MessageBox.Show("Chương trình gặp lỗi nghiêm trọng và không tiếp tục:" + "\r\n" + ex.Message, "Lỗi");
        }

        //private static void Bootstrap()
        //{
        //    // Create the container as usual.
        //    var container = new Container();

        //    // Register your types, for instance:
        //    container.Register<IUserRepository, SqlUserRepository>(Lifestyle.Singleton);
        //    container.Register<IUserContext, WinFormsUserContext>();
        //    container.Register<Form1>();

        //    // Optionally verify the container.
        //    container.Verify();
        //}

        private static void Test()
        {
            //var type = typeof(Officer);

            //var props = type.GetProperties();
            //var propStr = string.Join(", ", props.Select(p => p.Name));
            //Debug.WriteLine($"Props: {propStr}");
        }
        
        private static void TestDb()
        {
            var comp = db.Debts.First()?.Company;

            Debug.WriteLine($"comp: {comp?.Name}");
        }
    }
}
