﻿
Tool for converting Sql Server DB (including LocalDb) to SQLite.
https://www.codeproject.com/Articles/26932/Convert-SQL-Server-DB-to-SQLite-DB



## Dev Guide

- Imported Excel column order is matter.
- SoftArtisans.OfficeWriter must be installed from setup package to user machine.

### DB Design

- AppConfig: save app settings

  * ImportedMonths: imported months.
- BadDebt: Notifications by government offices regarding company events (missing, ...)

  * Công văn, Quyết định
- BadDebtType: Bad debt type

  * Chờ giải thể, Hết hoạt động, Mất tích, Nợ khác
- Company: company / organization
- Debt: imported debt information
- DebtRemind: debt reminding documentation
- Department: Governmental offices (công an, thuế)
- Officer: (cán bộ)
- PaperType: paper types that can be issued by Governmental offices

  * Update catalog form logic
- 
