﻿using DebtManagerWin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.ReportModels
{
    public class BadDebtReportModel
    {
        [ReportDataSetName("HEADER")]
        [ReportFieldName("REPORT_TITLE")]
        public string ReportTitle { get; set; }

        [ReportDataSetName("DATA")]
        public List<BadDebtReportRowModel> DataRows { get; set; }

        public BadDebtReportModel()
        {
            DataRows = new List<BadDebtReportRowModel>();
        }
    }

    public class BadDebtReportRowModel
    {
        [ReportFieldName("SERIAL_NO")]
        public int SerialNo { get; set; }

        [ReportFieldName("COMPANY_CODE")]
        public string CompanyCode { get; set; }

        [ReportFieldName("COMPANY_NAME")]
        public string CompanyName { get; set; }

        [ReportFieldName("PAPER_NUMBER")]
        public string PaperNumber { get; set; }
        
        [ReportFieldName("PAPER_DATE")]
        public DateTime PaperDate { get; set; }

        [ReportFieldName("DEPARTMENT")]
        public string Department { get; set; }

        [ReportFieldName("DEBT_MONTH")]
        public DateTime DebtMonth { get; set; }

        [ReportFieldName("DEBT_TYPE")]
        public string DebtType { get; set; }
    }
}
