﻿using DebtManagerWin.Models;
using DebtManagerWin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.ReportModels
{
    [ReportDataSetName("HEADER")]
    public class CompanyDebtProcessingReportModel
    {
        public int NoOfMonths { get; set; }

        [ReportFieldName("REPORT_TITLE")]
        public string ReportTitle { get; set; }

        [ReportFieldName("REPORT_TITLE2")]
        public string ReportTitle2 { get; set; }

        [ReportFieldName("MONTH")]
        public string[] Months { get; set; }

        /// <summary>
        /// Each outer list element match a month of NoOfMonths.
        /// Each inner list element store data for 1 row.
        /// </summary>
        public List<List<CompanyDebtProcessingReportRowModel>> MonthsData { get; set; }

        public CompanyDebtProcessingReportModel(int noOfMonths)
        {
            // Not exceed 12 months
            if (noOfMonths > AppConstants.MaxNoOfReportedMonths)
                throw new InvalidOperationException($"Số tháng báo cáo không thể vượt quá {AppConstants.MaxNoOfReportedMonths} tháng.");

            NoOfMonths = noOfMonths;
            
            MonthsData = new List<List<CompanyDebtProcessingReportRowModel>>();
            for (int i = 0; i < NoOfMonths; i++)
                MonthsData.Add(new List<CompanyDebtProcessingReportRowModel>());
        }
    }

    [ReportDataSetName("DATA")]
    public class CompanyDebtProcessingReportRowModel
    {
        public DateTime DebtMonth { get; set; }

        [ReportFieldName("NO_EMPLOYEES")]
        public int NoOfEmployees { get; set; }

        [ReportFieldName("PERIOD_START_DEBT")]
        public double PeriodStartDebt { get; set; }

        [ReportFieldName("PERIOD_DEBT")]
        public double PeriodDebt { get; set; }

        [ReportFieldName("PERIODS_DEBT_SI")]
        public double PeriodsDebtSI { get; set; }

        [ReportFieldName("PERIODS_DEBT_INTEREST")]
        public double PeriodsDebtInterest { get; set; }

        [ReportFieldName("REMIND")]
        public string Reminds { get; set; }

        [ReportFieldName("PAID_AMOUNT")]
        public double PaidAmount { get; set; }

        [ReportFieldName("PERIOD_END_DEBT")]
        public double PeriodEndDebt { get; set; }

        [ReportFieldName("PERIODE_DEBT_SI")]
        public double PeriodEDebtSI { get; set; }

        [ReportFieldName("PERIODE_DEBT_INTEREST")]
        public double PeriodEDebtInterest { get; set; }

        [ReportFieldName("DEBT_MONTHS")]
        public int DebtMonths { get; set; }

        [ReportFieldName("OFFICER")]
        public string Officer { get; set; }
    }
}
