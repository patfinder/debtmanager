﻿using DebtManagerWin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.ReportModels
{
    public class D04hReportModel
    {
        [ReportFieldName("PAPER_MONTH")]
        public string PaperMonth { get; set; }

        [ReportFieldName("COMPANY_NAME")]
        public string CompanyName { get; set; }

        [ReportFieldName("NO_EMPLOYEE")]
        public string NoOfEmployee { get; set; }

        [ReportFieldName("LEADER")]
        public string Leader { get; set; }

        [ReportFieldName("LEADER_TITLE")]
        public string LeaderTitle { get; set; }

        [ReportFieldName("ACCOUNTABLE")]
        public string Accountable { get; set; }

        [ReportFieldName("MONTH_END")]
        public string MonthEnd { get; set; }

        [ReportFieldName("DEBT_TOTAL")]
        public string DebtTotal { get; set; }

        [ReportFieldName("TOTAL_SALARY_FUND")]
        public string TotalSalaryFund { get; set; }

        [ReportFieldName("PAY_DEADLINE")]
        public string PayDeadline { get; set; }
    }
}
