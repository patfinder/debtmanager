﻿using DebtManagerWin.Models;
using DebtManagerWin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.ReportModels
{
    [ReportDataSetName("HEADER")]
    public class OldDebtProcessingReportModel
    {
        public int NoOfMonths { get; set; }

        [ReportFieldName("REPORT_TITLE")]
        public string ReportTitle { get; set; }

        public string ReportTitle2 { get; set; }

        [ReportFieldName("MONTH")]
        public string[] Months { get; set; }

        /// <summary>
        /// Each outer list element match a month of NoOfMonths.
        /// Each inner list element store data for 1 row.
        /// </summary>
        public List<List<CompanyDebtProcessingReportRowModel>> MonthsData { get; set; }

        public OldDebtProcessingReportModel(int noOfMonths)
        {
            // Not exceed 12 months
            if (noOfMonths > AppConstants.MaxNoOfReportedMonths)
                throw new InvalidOperationException($"Số tháng báo cáo không thể vượt quá {AppConstants.MaxNoOfReportedMonths} tháng.");

            NoOfMonths = noOfMonths;
            
            MonthsData = new List<List<CompanyDebtProcessingReportRowModel>>();
            for (int i = 0; i < NoOfMonths; i++)
                MonthsData.Add(new List<CompanyDebtProcessingReportRowModel>());
        }
    }

    [ReportDataSetName("DATA")]
    public class OldDebtProcessingReportRowModel
    {
        public DateTime DebtMonth { get; set; }

        [ReportFieldName("START_NO_DEBT_COMPANY")]
        public int StartNoOfDebtCompany { get; set; }

        [ReportFieldName("START_NO_DEBT_COMPANY_2M")]
        public int StartNoOfDebtCompany2M { get; set; }

        [ReportFieldName("START_NO_DEBT_COMPANY_6M")]
        public int StartNoOfDebtCompany6M { get; set; }

        [ReportFieldName("START_NO_DEBT_COMPANY_6PM")]
        public int StartNoOfDebtCompany6pM { get; set; }

        [ReportFieldName("START_TOTAL_DEBT")]
        public double StartTotalDebt { get; set; }

        [ReportFieldName("START_TOTAL_DEBT_2M")]
        public double StartTotalDebt2M { get; set; }

        [ReportFieldName("START_TOTAL_DEBT_6M")]
        public double StartTotalDebt6M { get; set; }

        [ReportFieldName("START_TOTAL_DEBT_6PM")]
        public double StartTotalDebt6pM { get; set; }

        [ReportFieldName("MID_TOTAL_DEBT")]
        public double MidTotalDebt { get; set; }

        [ReportFieldName("MID_TOTAL_DEBT_2M")]
        public double MidTotalDebt2M { get; set; }

        [ReportFieldName("MID_TOTAL_DEBT_6M")]
        public double MidTotalDebt6M { get; set; }

        [ReportFieldName("MID_TOTAL_DEBT_6PM")]
        public double MidTotalDebt6pM { get; set; }

        [ReportFieldName("REMIND")]
        public string Reminds { get; set; }

        [ReportFieldName("PAID_DEBT")]
        public double PaidDebt { get; set; }

        [ReportFieldName("END_TOTAL_DEBT")]
        public double EndTotalDebt { get; set; }

        [ReportFieldName("END_TOTAL_DEBT_2M")]
        public double EndTotalDebt2M { get; set; }

        [ReportFieldName("END_TOTAL_DEBT_6M")]
        public double EndTotalDebt6M { get; set; }

        [ReportFieldName("END_TOTAL_DEBT_6PM")]
        public double EndTotalDebt6pM { get; set; }


    }
}
