﻿using DebtManagerWin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.ReportModels
{
    public class RemindDebtReportModel
    {
        [ReportDataSetName("HEADER")]
        [ReportFieldName("REPORT_TITLE")]
        public string ReportTitle { get; set; }

        [ReportDataSetName("DATA")]
        public List<RemindDebtReportRowModel> DataRows { get; set; }

        public RemindDebtReportModel()
        {
            DataRows = new List<RemindDebtReportRowModel>();
        }
    }

    public class RemindDebtReportRowModel
    {
        [ReportFieldName("SERIAL_NO")]
        public int SerialNo { get; set; }

        [ReportFieldName("COMPANY_CODE")]
        public string CompanyCode { get; set; }

        [ReportFieldName("COMPANY_NAME")]
        public string CompanyName { get; set; }

        [ReportFieldName("COMPANY_ADDRESS")]
        public string CompanyAddress { get; set; }

        [ReportFieldName("OFFICER")]
        public string Officer { get; set; }

        [ReportFieldName("DEBT_MONTHS")]
        public int DebtMonths { get; set; }

        // 3 New fields - Nov 2018
        [ReportFieldName("LAST_MONTH_DEBT")]
        public int LastMonthDebt { get; set; }

        [ReportFieldName("THIS_MONTH_PAID")]
        public int ThisMonthPaid { get; set; }

        [ReportFieldName("THIS_MONTH_COLLECT")]
        public int ThisMonthCollect { get; set; }

        [ReportFieldName("DEBT_REMINDS")]
        public string DebtReminds { get; set; }

        //[ReportFieldName("HAS_D04H")]
        //public ReportBool HasD04H { get; set; }

        //[ReportFieldName("HAS_BIENBAN1")]
        //public ReportBool HasNotification1 { get; set; }

        //[ReportFieldName("HAS_BIENBAN2")]
        //public ReportBool HasNotification2 { get; set; }

        [ReportFieldName("SENT_TO_KTTN")]
        public ReportBool SentToKttn { get; set; }
    }
}
