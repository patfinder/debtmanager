﻿using System.Collections.Generic;

namespace DebtManagerWin.ReportModels
{
    public class RemindDebtReportSummaryModel
    {
        [ReportDataSetName("DATA2")]
        public List<RemindDebtReportSummaryRowModel> DataRows { get; set; }

        public RemindDebtReportSummaryModel()
        {
            DataRows = new List<RemindDebtReportSummaryRowModel>();
        }
    }

    public class RemindDebtReportSummaryRowModel
    {
        [ReportFieldName("OFFICER")]
        public string Officer { get; set; }

        [ReportFieldName("COMPANY_COUNT_1")]
        public int Count1 { get; set; }

        [ReportFieldName("TOTAL_DEBT_1")]
        public double Debt1 { get; set; }

        [ReportFieldName("TOTAL_PAID_1")]
        public double Paid1 { get; set; }

        [ReportFieldName("TOTAL_COLLECT_1")]
        public double Collect1 { get; set; }


        [ReportFieldName("COMPANY_COUNT_2")]
        public int Count2 { get; set; }

        [ReportFieldName("TOTAL_DEBT_2")]
        public double Debt2 { get; set; }

        [ReportFieldName("TOTAL_PAID_2")]
        public double Paid2 { get; set; }

        [ReportFieldName("TOTAL_COLLECT_2")]
        public double Collect2 { get; set; }


        [ReportFieldName("COMPANY_COUNT_3")]
        public int Count3 { get; set; }

        [ReportFieldName("TOTAL_DEBT_3")]
        public double Debt3 { get; set; }

        [ReportFieldName("TOTAL_PAID_3")]
        public double Paid3 { get; set; }

        [ReportFieldName("TOTAL_COLLECT_3")]
        public double Collect3 { get; set; }
    }
}
