﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.ReportModels
{
    /// <summary>
    /// Define a boolean type to display user friendly string for the value
    /// </summary>
    public class ReportBool
    {
        public bool Value { get; set; }

        public static implicit operator ReportBool(bool b)
        {
            return new ReportBool { Value = b };
        }

        public override string ToString() => Value ? "Có" : "Không";
    }
}
