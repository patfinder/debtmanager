﻿using System;

namespace DebtManagerWin.ReportModels
{
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Property, Inherited = true)]
    public class ReportDataSetName : Attribute
    {
        public string Name { get; set; }

        public ReportDataSetName(string name)
        {
            Name = name;
        }
    }

}
