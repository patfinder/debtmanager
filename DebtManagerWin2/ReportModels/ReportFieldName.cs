﻿using SmartDev.Libs;
using System;

namespace DebtManagerWin.ReportModels
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class ReportFieldName : Attribute, IAttribute<string>
    {
        public string Value { get; set; }

        public ReportFieldName(string name)
        {
            Value = name;
        }
    }
}
