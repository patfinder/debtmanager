﻿using System;
using System.Configuration;

namespace DebtManagerWin.Services
{
    public static class AppConstants
    {
        public static DateTime MinDate => new DateTime(1970, 1, 1);

        public static DateTime ValidDate => new DateTime(DateTime.Now.Year - 10, 1, 1);

        public static string DateFormat => "dd/MM/yyyy";

        public static string MonthFormat => "MM/yyyy";

        /// <summary>
        /// List page size
        /// </summary>
        public static int RowsToDisplay => int.Parse(ConfigurationManager.AppSettings["RowsToDisplay"] ?? "20");

        public static int MonthsToShow => int.Parse(ConfigurationManager.AppSettings["MonthsToShow"] ?? "12");

        public static int MaxNoOfReportedMonths => 12; // Fixed in report template.

        /// <summary>
        /// Số tháng nợ bắt đầu cảnh báo (D04H)
        /// </summary>
        public static int AlarmDebtMonths => int.Parse(ConfigurationManager.AppSettings["AlarmDebtMonths"] ?? "2");

    }
}
