﻿using DebtManagerWin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.DataServices
{
    public class DebtDbContext : DbContext
    {
        private static string GetConnectionString()
        {
            // Build connection string using default DB path of colocation with app executable.

            // Check if the path exists.
            var dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AppData\\LocalDataV11.mdf");
            if (!File.Exists(dbPath))
            {
                return null;
            }

            var sqlString = new SqlConnectionStringBuilder()
            {
                //DataSource = "SOURAV-PC", // Server name
                //InitialCatalog = "efDB",  //Database
                //UserID = "sourav",         //Username
                //Password = "mypassword",  //Password
                DataSource = "(LocalDB)\v11.0",
                AttachDBFilename = dbPath,
                IntegratedSecurity = true,
                ConnectTimeout = 30,
            };

            var entityString = new EntityConnectionStringBuilder()
            {
                //Provider = "System.Data.SqlClient",
                //Metadata = "res://*/testModel.csdl|res://*/testModel.ssdl|res://*/testModel.msl",
                ProviderConnectionString = sqlString.ToString()
            };

            return entityString.ConnectionString;
        }

        public DebtDbContext() : base(
            //GetConnectionString() ?? 
            "DebtDbContext")
        {
		    // Ignore db initializer
		    Database.SetInitializer<DebtDbContext>(null);
        }

        public DbSet<AppConfig> AppConfigs { get; set; }

        public DbSet<GovernmentalDocument> GovernmentalDocuments { get; set; }

        public DbSet<BadDebtType> BadDebtTypes { get; set; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<Debt> Debts { get; set; }

        public DbSet<DebtRemind> DebtReminds { get; set; }

        public DbSet<Department> Departments { get; set; }

        public DbSet<PaperType> PaperTypes { get; set; }

        public DbSet<Officer> Officers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //BadDebtType
            modelBuilder.Entity<BadDebtType>()
                .HasIndex(p => p.Name)
                .IsUnique();

            //Company
            modelBuilder.Entity<Company>()
                .HasIndex(p => p.Code)
                .IsUnique();

            // Debt
            modelBuilder.Entity<Debt>()
                .HasIndex(p => new { p.ImportMonth })
                .IsUnique();

            modelBuilder.Entity<Debt>()
                .HasIndex(p => new { p.CompanyId, p.ImportMonth })
                .IsUnique();

            // DebtRemind
            modelBuilder.Entity<DebtRemind>()
                .HasIndex(p => new { p.CompanyId, p.PaperTypeId, p.PaperNumber })
                .IsUnique();

            // Department
            modelBuilder.Entity<Department>()
                .HasIndex(p => p.Name)
                .IsUnique();

            // PaperType
            modelBuilder.Entity<PaperType>()
                .HasIndex(p => p.Name)
                .IsUnique();

            // Officer
            modelBuilder.Entity<Officer>()
                .HasIndex(p => p.StaffId)
                .IsUnique();



            //modelBuilder.Entity<BadDebt>().Map(m =>
            //{
            //    m.MapInheritedProperties();
            //    m.ToTable(nameof(BadDebt));
            //});
        }
    }
}
