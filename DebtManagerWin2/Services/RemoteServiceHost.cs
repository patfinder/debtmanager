﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Services
{
    public static class RemoteServiceHost
    {
        public static void Start()
        {
            RemotingConfiguration.Configure(null, false);
        }
    }
}
