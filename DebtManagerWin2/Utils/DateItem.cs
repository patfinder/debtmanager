﻿using System;

namespace DebtManagerWin.Utils
{
    public class DateListItem
    {
        public DateTime Value { get; set; }

        public string Format { get; set; }

        public string Display => Value.ToString(Format);
    }
}
