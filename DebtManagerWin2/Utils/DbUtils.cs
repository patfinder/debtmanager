﻿using DebtManagerWin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.DataServices
{
    public static class DbUtils
    {
        public static void UpdateToDb<T>(
            DebtDbContext db,
            List<DanhMucItem> items,
            DbSet<T> sourceList,
            Action<DbSet<T>, DanhMucItem> addToDb,
            Action<T, DanhMucItem> updateToDb) where T : class
        {
            // Add new
            items.Where(i => i.Id == 0).ToList().ForEach(i =>
            {
                addToDb(sourceList, i);
            });

            // Update
            items.Where(i => i.Id != 0 && i.Status == DanhMucItemStatus.Updated).ToList().ForEach(i =>
            {
                updateToDb(sourceList.ToList().First(o => i.Id == GetId(o)), i);
            });

            // Delete
            // For Officer only
            // TODO: kind of hard-code
            if(typeof(T).Name == nameof(Officer))
            {
                items.Where(i => i.Status == DanhMucItemStatus.Deleted).ToList().ForEach(i =>
                {
                    // Remove links from Company, Doc, DebtRemind
                    var item = sourceList.ToList().First(dbItem => i.Id == GetId(dbItem));
                    var officer = item as Officer;

                    officer.GovernmentalDocuments.ToList().ForEach(d => d.OfficerId = null);
                    officer.DebtReminds.ToList().ForEach(d => d.OfficerId = null);

                    sourceList.Remove(item);
                });
            }
        }

        public static int GetId(object obj) => ((dynamic)obj).Id;

        public static int SetId(object obj, int id) => ((dynamic)obj).Id = id;

        public static string GetName(object obj) => ((dynamic)obj).Name;

        public static string SetName(object obj, string name) => ((dynamic)obj).Name = name;
    }
}
