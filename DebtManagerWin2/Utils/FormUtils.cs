﻿using DebtManagerWin.DataServices;
using DebtManagerWin.Models;
using SmartDev.Libs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;
using RxUtils = SmartDev.Libs.ReflectionUtils;

namespace DebtManagerWin.Utils
{
  public static class FormUtils
    {
        /// <summary>
        /// Update with basic update actions (Name, Id).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="db"></param>
        /// <param name="dbSet"></param>
        /// <param name="newT">Func to create new T</param>
        /// <param name="catalogName">Tên danh mục. Eg: "Cán bộ", "Loại công văn"</param>
        public static Form UpdateCatalog<T>(Form mdiParent, Action<object, FormClosedEventArgs> formClosed, DebtDbContext db, 
            DbSet<T> dbSet, Func<int, string> getMoreInfo, Func<int, bool> canDelete, List<T> sourceItems, Func<T> newT, string catalogName) where T : class // CatalogBasedType
        {
            void addAction(DbSet<T> dbSet1, DanhMucItem item)
            {
                var newCat = newT();
                DbUtils.SetName(newCat, item.Fields[0]);
                dbSet1.Add(newCat);
            }

            void updateAction(T dbItem, DanhMucItem item)
            {
                DbUtils.SetName(dbItem, item.Fields[0]);
            }

            string[] getFieldValues(T dbItem) => new[] { DbUtils.GetName(dbItem) };
            var fieldNames = new[]
            {
                // TODO: CatalogBasedType
                AttributeUtils.GetDisplayName(typeof(CatalogBasedType).GetMemberInfo("Name")),
            };

            return UpdateCatalog(mdiParent, formClosed, db, dbSet, getMoreInfo, canDelete, sourceItems, addAction, updateAction, getFieldValues, catalogName, fieldNames);
        }
        
        /// <summary>
        /// Update catalog function. Include parameters for all supported cases.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="mdiParent"></param>
        /// <param name="formClosed"></param>
        /// <param name="db"></param>
        /// <param name="dbSet"></param>
        /// <param name="sourceItems"></param>
        /// <param name="addAction"></param>
        /// <param name="updateAction"></param>
        /// <param name="getFieldValues"></param>
        /// <param name="catalogName"></param>
        /// <param name="fieldNames"></param>
        /// <returns></returns>
        public static Form UpdateCatalog<T>(Form mdiParent, Action<object, FormClosedEventArgs> formClosed, 
            DebtDbContext db, DbSet<T> dbSet, Func<int, string> getMoreInfo, Func<int, bool> canDelete, List<T> sourceItems, Action<DbSet<T>, DanhMucItem> addAction, 
            Action<T, DanhMucItem> updateAction, Func<T, string[]> getFieldValues, string catalogName, string[] fieldNames) 
            where T : class
        {
            var items = new List<DanhMucItem>(sourceItems.Count());

            // Load data
            sourceItems.ForEach(dbItem =>
            {
                items.Add(new DanhMucItem
                {
                    Id = DbUtils.GetId(dbItem),
                    Status = DanhMucItemStatus.Existing,
                    Fields = getFieldValues(dbItem)
                });
            });



            // TODO: DESIGN
            //return null;

            var frmDanhMuc = new FrmUpdateCatalog<T>(catalogName, dbSet, getMoreInfo, canDelete, items, fieldNames)
            {
                MdiParent = mdiParent
            };

            if (formClosed != null)
                frmDanhMuc.FormClosed += new FormClosedEventHandler(formClosed);

            // Handle submit (Click OK)
            frmDanhMuc.OnFormCloseOk += (dbSet1) =>
            {
                // Update back to list.
                DbUtils.UpdateToDb(db, frmDanhMuc.ItemList, dbSet1,
                    (dbSet2, item) => addAction(dbSet2, item),
                    (dbItem, item) => updateAction(dbItem, item));

                db.SaveChanges();
            };

            frmDanhMuc.Show();

            return frmDanhMuc;
        }

        /// <summary>
        /// See UpdateOfficer
        /// </summary>
        /// <param name="mdiParent"></param>
        /// <param name="db"></param>
        /// <param name="formClosed"></param>
        /// <returns></returns>
        public static Form UpdateCompany(Form mdiParent, DebtDbContext db, Action<object, FormClosedEventArgs> formClosed)
        {
            void addAction(DbSet<Company> dbSet, DanhMucItem item)
            {
                throw new Exception("Thao tác thêm mới thông tin công ty chưa được hỗ trợ.");

                //dbSet.Add(new Company
                //{
                //    Name = item.Fields[0],
                //    Code = item.Fields[1],
                //    Officer = null,
                //    LastDebtUpdate = DateTime.Now,
                //    MonthsOfDebt = 0,
                //});
            }

            void updateAction(Company dbItem, DanhMucItem item)
            {
                dbItem.Name = item.Fields[0];
                dbItem.Code = item.Fields[1];
            }
            string[] getFieldValues(Company dbItem) => new[] { dbItem.Name, dbItem.Code };

            var catalogName = AttributeUtils.GetDisplayName<Company>();

            var fieldNames = new[]
            {
                AttributeUtils.GetDisplayName(RxUtils.GetMemberInfo<Company>(nameof(Company.Name))),
                AttributeUtils.GetDisplayName(RxUtils.GetMemberInfo<Company>(nameof(Company.Code))),
            };

            return UpdateCatalog(mdiParent, formClosed, db, db.Companies, null, (id) => false, db.Companies.Take(100).ToList(), addAction, updateAction, getFieldValues, catalogName, fieldNames);
        }

        /// <summary>
        /// Func to handle showing form & calling form event hanlders.
        /// </summary>
        /// <param name="mdiParent">Pointer to parent form. Needed for MDI form.</param>
        /// <param name="db">DB context</param>
        /// <param name="formClosed">Callback func when the form is closed</param>
        /// <returns></returns>
        public static Form UpdateOfficer(Form mdiParent, DebtDbContext db, Action<object, FormClosedEventArgs> formClosed)
        {
            Action<DbSet<Officer>, DanhMucItem> addAction = (DbSet<Officer> dbSet, DanhMucItem item) =>
            {
                dbSet.Add(new Officer
                {
                    Name = item.Fields[0],
                    StaffId = item.Fields[1],
                    //FullName = item.Fields[1],
                });
            };

            Action<Officer, DanhMucItem> updateAction = (Officer dbItem, DanhMucItem item) =>
            {
                dbItem.Name = item.Fields[0];
                dbItem.StaffId = item.Fields[1];
                //dbItem.FullName = item.Fields[1];
            };

            // dbItem.FullName, 
            string[] getFieldValues(Officer dbItem) => new[] { dbItem.Name, dbItem.StaffId, };

            var catalogName = AttributeUtils.GetDisplayName<Officer>();

            var fieldNames = new[]
            {
                AttributeUtils.GetDisplayName(RxUtils.GetMemberInfo<Officer>(nameof(Officer.Name))),
                AttributeUtils.GetDisplayName(RxUtils.GetMemberInfo<Officer>(nameof(Officer.StaffId))),
                //AttributeUtils.GetDisplayName(RxUtils.GetMemberInfo<Officer>(nameof(Officer.FullName))),
            };

            Func<int, string> getMoreInfo = (id) =>
            {
                var item = db.Officers.FirstOrDefault(o => o.Id == id);

                if (item == null)
                    return null;

                var companies = string.Join("\r\n", item.Companies.OrderBy(c => c.Name).Select(c => $"{c.Name} ({c.Code})"));
                return $"Đơn vị được quản lý:\r\n{companies}";
            };

            //Func<int, bool> canDelete = (id) => db.Officers.FirstOrDefault(o => o.Id == id)?.Companies.Any() ?? true;

            return UpdateCatalog(mdiParent, formClosed, db, db.Officers, getMoreInfo, (id) => true, 
                db.Officers.ToList(), addAction, updateAction, getFieldValues, catalogName, fieldNames);
        }

        /// <summary>
        /// See UpdateOfficer
        /// </summary>
        /// <param name="mdiParent"></param>
        /// <param name="db"></param>
        /// <param name="formClosed"></param>
        /// <param name="paperGroup"></param>
        /// <returns></returns>
        public static Form UpdatePaperType(Form mdiParent, DebtDbContext db, Action<object, FormClosedEventArgs> formClosed, PaperGroup paperGroup)
        {
            Func<PaperType> newFunc = () =>
            {
                return new PaperType { PaperGroup = paperGroup };
            };
            
            var catalogName = AttributeUtils.GetDisplayName<PaperType>() + " - " + (paperGroup == PaperGroup.CompanyNotification ? "Biên bản" : "Công văn");

            //Func<int, bool> canDelete = (id) => {
            //    var paperType = db.PaperTypes.FirstOrDefault(o => o.Id == id);

            //    if (paperType == null)
            //        return true;

            //    return paperType.PaperGroup == PaperGroup.DebtReminding && !db.DebtReminds.Any(d => d.PaperType == paperType) ||
            //        paperType.PaperGroup == PaperGroup.CompanyNotification && !db.BadDebts.Any(d => d.PaperType == paperType);
            //};

            return UpdateCatalog(mdiParent, formClosed, db, db.PaperTypes, null, (id) => false, 
                db.PaperTypes.Where(d => d.PaperGroup == paperGroup).ToList(), newFunc, catalogName);
        }

        /// <summary>
        /// See UpdateOfficer
        /// </summary>
        /// <param name="mdiParent"></param>
        /// <param name="db"></param>
        /// <param name="formClosed"></param>
        /// <returns></returns>
        public static Form UpdateDepartment(Form mdiParent, DebtDbContext db, Action<object, FormClosedEventArgs> formClosed)
        {
            var catalogName = AttributeUtils.GetDisplayName<Department>();

            //Func<int, bool> canDelete = (id) => {
            //    var item = db.Departments.FirstOrDefault(o => o.Id == id);

            //    if (item == null)
            //        return true;

            //    return !db.DebtReminds.Any(d => d.Department == item) ||
            //        !db.BadDebts.Any(d => d.Department == item);
            //};

            return UpdateCatalog(mdiParent, formClosed, db, db.Departments, null, (id) => false, 
                db.Departments.ToList(), () => new Department(), catalogName);
        }

        /// <summary>
        /// See UpdateOfficer
        /// </summary>
        /// <param name="mdiParent"></param>
        /// <param name="db"></param>
        /// <param name="formClosed"></param>
        /// <returns></returns>
        public static Form UpdateDebtType(Form mdiParent, DebtDbContext db, Action<object, FormClosedEventArgs> formClosed)
        {
            var catalogName = AttributeUtils.GetDisplayName<BadDebtType>();

            //Func<int, bool> canDelete = (id) => {
            //    var item = db.BadDebtTypes.FirstOrDefault(o => o.Id == id);

            //    if (item == null)
            //        return true;

            //    return !db.BadDebts.Any(d => d.BadDebtType == item);
            //};

            return UpdateCatalog(mdiParent, formClosed, db, db.BadDebtTypes, null, (id) => false, 
                db.BadDebtTypes.ToList(), () => new BadDebtType(), catalogName);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="mdiForm"></param>
        /// <param name="formClosed"></param>
        /// <returns></returns>
        public static T ShowMdiChild<T>(Form mdiForm, Action formClosed) where T : Form, new()
        {
            return ShowMdiChild<T>(mdiForm, (obj, args) => formClosed());
        }

        public static T ShowMdiChild<T>(Form mdiParent, Action<object, FormClosedEventArgs> formClosed) where T : Form, new()
        {
            var childDialog = new T()
            {
                MdiParent = mdiParent
            };

            childDialog.FormClosed += new FormClosedEventHandler(formClosed);
            childDialog.Show();

            return childDialog;
        }
    }
}
