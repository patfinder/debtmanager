﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Utils
{
    public static class IdentityServices
    {
        public static string NewLowGuidTimestamp()
        {
            return Guid.NewGuid().ToString().Substring(0, 8);
        }
    }
}
