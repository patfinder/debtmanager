﻿using DebtManagerWin.ReportModels;
using SmartDev.Libs;
using RxUtils = SmartDev.Libs.ReflectionUtils;

namespace DebtManagerWin.Utils
{
  public static class ReflectionUtils
    {
        public static string GetReportFieldName<T>(string memberName)
        {
            return AttributeUtils.GetAttribute<ReportFieldName>(RxUtils.GetMemberInfo<T>(memberName)).Value;
        }
    }
}
