﻿using DebtManagerWin.DataServices;
using DebtManagerWin.ReportModels;
using DebtManagerWin.Services;
using log4net;
using SmartDev.Libs;
using SoftArtisans.OfficeWriter.ExcelWriter;
using SoftArtisans.OfficeWriter.WordWriter;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using SDRxUtils = SmartDev.Libs.ReflectionUtils;
using RxUtils = DebtManagerWin.Utils.ReflectionUtils;
using Newtonsoft.Json;
using System.Data.Entity;
using DebtManagerWin.Models;

namespace DebtManagerWin.Utils
{
    public static class ReportUtils
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Program));

        private static DebtDbContext db => Program.db;

        /// <summary>
        /// Get names & values
        /// </summary>
        /// <returns></returns>
        private static string[] GetDebtReportFieldNames()
        {
            return new[] {
                AttributeUtils.GetAttribute<BadDebtReportRowModel, ReportFieldName>(nameof(BadDebtReportRowModel.SerialNo)).Value,
                AttributeUtils.GetAttribute<BadDebtReportRowModel, ReportFieldName>(nameof(BadDebtReportRowModel.CompanyCode)).Value,
                AttributeUtils.GetAttribute<BadDebtReportRowModel, ReportFieldName>(nameof(BadDebtReportRowModel.CompanyName)).Value,
                AttributeUtils.GetAttribute<BadDebtReportRowModel, ReportFieldName>(nameof(BadDebtReportRowModel.PaperNumber)).Value,
                AttributeUtils.GetAttribute<BadDebtReportRowModel, ReportFieldName>(nameof(BadDebtReportRowModel.PaperDate)).Value,
                AttributeUtils.GetAttribute<BadDebtReportRowModel, ReportFieldName>(nameof(BadDebtReportRowModel.Department)).Value,
                AttributeUtils.GetAttribute<BadDebtReportRowModel, ReportFieldName>(nameof(BadDebtReportRowModel.DebtMonth)).Value,
                AttributeUtils.GetAttribute<BadDebtReportRowModel, ReportFieldName>(nameof(BadDebtReportRowModel.DebtType)).Value,
            };
        }

        /// <summary>
        /// Báo cáo Nợ khó thu
        /// </summary>
        /// <param name="templatePath"></param>
        /// <param name="outputFile"></param>
        /// <param name="paperDate"></param>
        /// <returns></returns>
        public static bool GenerateBadDebtReport(string templatePath, string outputFile, DateTime paperDate)
        {
            try
            {
                var xlsTemplate = new ExcelTemplate();
                xlsTemplate.Open(templatePath);

                var dataFieldNames = GetDebtReportFieldNames();
                int rowNo = 1;

                // Select Jan 01 then filter for month <= Feb 01
                var paperDateQuery = paperDate.AddMonths(1);
                var query = db.GovernmentalDocuments.Where(d => d.PaperDate <= paperDateQuery).OrderBy(d => d.Company.Name);

                _logger.Info($"{nameof(GenerateBadDebtReport)} (Báo cáo Nợ khó thu) - templatePath: {templatePath}, outputFile: {outputFile}, paperDate: {paperDate}, query: {query}");

                var reportModel = new BadDebtReportModel()
                {
                    ReportTitle = $"BÁO CÁO ĐƠN VỊ NỢ KHÓ THU - Tháng {paperDate.ToString(AppConstants.MonthFormat)}",
                    DataRows = query.ToList().Select(d =>
                        new BadDebtReportRowModel
                        {
                            CompanyCode = d.Company.Code,
                            CompanyName = d.Company.Name,
                            DebtMonth = d.DebtMonth,
                            DebtType = d.BadDebtType.Name,
                            Department = d.Department.Name,
                            PaperDate = d.PaperDate,
                            PaperNumber = d.PaperNumber,
                            SerialNo = rowNo++,
                        }).ToList(),
                };

                // Convert to dataset
                var dataTable = new DataTable();

                // name-column mapping
                var rowType = typeof(BadDebtReportRowModel);
                var propInfos = rowType.GetProperties();

                var nameCols = new Dictionary<string, DataColumn>();
                var nameInfos = new Dictionary<string, PropertyInfo>();
                dataTable.Columns.AddRange(dataFieldNames.Select(f => {
                    var col = new DataColumn(f);
                    nameCols.Add(f, col);
                    nameInfos.Add(f, propInfos.First(p => RxUtils.GetReportFieldName<BadDebtReportRowModel>(p.Name) == f));
                    return col;
                }).ToArray());

                // loop on rows
                reportModel.DataRows.ForEach(row =>
                {
                    var dataRow = dataTable.NewRow();
                    // loop on fields
                    dataFieldNames.ToList().ForEach(field =>
                    {
                        dataRow.SetField(nameCols[field], SDRxUtils.GetPropertyValue(row, nameInfos[field]));
                    });
                    dataTable.Rows.Add(dataRow);
                });

                var dataProps = xlsTemplate.CreateDataBindingProperties();

                // Bind header
                var fieldNames = new[] { AttributeUtils.GetAttribute<BadDebtReportModel, ReportFieldName>(nameof(BadDebtReportModel.ReportTitle)).Value };
                var fieldValues = new[] { reportModel.ReportTitle };
                var dataSetName = AttributeUtils.GetAttribute<BadDebtReportModel, ReportDataSetName>(nameof(BadDebtReportModel.ReportTitle)).Name;

                xlsTemplate.BindRowData(fieldValues, fieldNames, dataSetName, dataProps);

                // Bind rows
                var rowDatasetName = AttributeUtils.GetAttribute<BadDebtReportModel, ReportDataSetName>(nameof(BadDebtReportModel.DataRows)).Name;
                xlsTemplate.BindData(dataTable, rowDatasetName, dataProps);

                xlsTemplate.Process();
                xlsTemplate.Save(outputFile);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(GenerateBadDebtReport)} error: {ex.Message} - {ex.StackTrace}");
                //MessageBox.Show($"Lỗi: {ex.Message}", "Lỗi sinh report");
                return false;
            }
        }

        /// <summary>
        /// BC D04H
        /// </summary>
        /// <param name="reportModel"></param>
        /// <param name="outputPath"></param>
        /// <returns></returns>
        public static bool GenerateD04hDocument(D04hReportModel reportModel, string outputPath)
        {
            string templatePath = "";

            try
            {
                var wordTemplate = new WordTemplate();

                _logger.Info($"{nameof(GenerateD04hDocument)} (Báo cáo D04H) - outputPath: {outputPath}, reportModel: {JsonConvert.SerializeObject(reportModel)}");

                templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates", "D04h.doc");
                wordTemplate.Open(templatePath);

                wordTemplate.SetDataSource(
                    new object[] {
                        reportModel.PaperMonth,
                        reportModel.CompanyName,
                        reportModel.NoOfEmployee,
                        reportModel.Leader,
                        reportModel.LeaderTitle,
                        reportModel.Accountable,
                        reportModel.MonthEnd,
                        reportModel.DebtTotal,
                        reportModel.TotalSalaryFund,
                        reportModel.PayDeadline,
                    },
                    new string[] {
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.PaperMonth)),
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.CompanyName)),
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.NoOfEmployee)),
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.Leader)),
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.LeaderTitle)),
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.Accountable)),
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.MonthEnd)),
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.DebtTotal)),
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.TotalSalaryFund)),
                        RxUtils.GetReportFieldName<D04hReportModel>(nameof(D04hReportModel.PayDeadline)),
                    });

                wordTemplate.Process();
                wordTemplate.Save(outputPath);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(GenerateD04hDocument)} error: {templatePath} - Report Model: {JsonConvert.SerializeObject(reportModel)} - Error: {ex.Message} - {ex.StackTrace}");
                return false;
            }
        }

        private static Dictionary<string, string> GetDebtRemindReportFieldNames()
        {
            return new Dictionary<string, string>
            {
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.SerialNo)).Value,         nameof(RemindDebtReportRowModel.SerialNo        )},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.CompanyCode)).Value,      nameof(RemindDebtReportRowModel.CompanyCode     )},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.CompanyName)).Value,      nameof(RemindDebtReportRowModel.CompanyName     )},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.CompanyAddress)).Value,   nameof(RemindDebtReportRowModel.CompanyAddress  )},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.Officer)).Value,          nameof(RemindDebtReportRowModel.Officer         )},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.DebtMonths)).Value,       nameof(RemindDebtReportRowModel.DebtMonths      )},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.LastMonthDebt)).Value,    nameof(RemindDebtReportRowModel.LastMonthDebt   )},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.ThisMonthPaid)).Value,    nameof(RemindDebtReportRowModel.ThisMonthPaid   )},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.ThisMonthCollect)).Value, nameof(RemindDebtReportRowModel.ThisMonthCollect)},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.DebtReminds)).Value,      nameof(RemindDebtReportRowModel.DebtReminds     )},
                //{AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.HasD04H)).Value,          nameof(RemindDebtReportRowModel.HasD04H         )},
                //{AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.HasNotification1)).Value, nameof(RemindDebtReportRowModel.HasNotification1)},
                //{AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.HasNotification2)).Value, nameof(RemindDebtReportRowModel.HasNotification2)},
                {AttributeUtils.GetAttribute<RemindDebtReportRowModel, ReportFieldName>(nameof(RemindDebtReportRowModel.SentToKttn)).Value,       nameof(RemindDebtReportRowModel.SentToKttn      )},
            };
        }

        private static Dictionary<string, string> GetDebtRemindSummaryReportFieldNames()
        {
            return new Dictionary<string, string>
            {
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Officer)).Value,    nameof(RemindDebtReportSummaryRowModel.Officer   )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Count1)).Value,     nameof(RemindDebtReportSummaryRowModel.Count1    )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Debt1)).Value,      nameof(RemindDebtReportSummaryRowModel.Debt1     )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Paid1)).Value,      nameof(RemindDebtReportSummaryRowModel.Paid1     )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Collect1)).Value,   nameof(RemindDebtReportSummaryRowModel.Collect1  )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Count2)).Value,     nameof(RemindDebtReportSummaryRowModel.Count2    )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Debt2)).Value,      nameof(RemindDebtReportSummaryRowModel.Debt2     )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Paid2)).Value,      nameof(RemindDebtReportSummaryRowModel.Paid2     )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Collect2)).Value,   nameof(RemindDebtReportSummaryRowModel.Collect2  )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Count3)).Value,     nameof(RemindDebtReportSummaryRowModel.Count3    )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Debt3)).Value,      nameof(RemindDebtReportSummaryRowModel.Debt3     )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Paid3)).Value,      nameof(RemindDebtReportSummaryRowModel.Paid3     )},
                {AttributeUtils.GetAttribute<RemindDebtReportSummaryRowModel, ReportFieldName>(nameof(RemindDebtReportSummaryRowModel.Collect3)).Value,   nameof(RemindDebtReportSummaryRowModel.Collect3  )},
            };
        }

        class GroupInfo
        {
            public string Office { get; set; }
            public int Count { get; set; }
            public double Debt { get; set; }
            public double Paid { get; set; }

            public double Collected { get; set; }

            public GroupInfo()
            {
            }

            public GroupInfo(string officer)
            {
                this.Office = Office;
            }
        }


        /// <summary>
        /// Báo cáo Đôn đốc nợ Hoặc Bàn giao Kĩ thuật thu nợ.
        /// </summary>
        /// <param name="templatePath"></param>
        /// <param name="outputFile"></param>
        /// <param name="reportMonth">First date of the reported month.</param>
        /// <returns></returns>
        public static bool GenerateDebtRemindReport(bool sendToKttn, string templatePath, string outputFile, DateTime reportMonth, DebtRemindReportType reportType)
        {
            // TODO: notifications (D04H++) of Jan debts is supposed to be created in Feb (or Jan also)
            // So the alarm for Jan report should also include notifications created in next month
            // Confirm this !!

            try
            {
                var xlsTemplate = new ExcelTemplate();
                xlsTemplate.Open(templatePath);

                // Get start of month
                reportMonth = new DateTime(reportMonth.Year, reportMonth.Month, 1);

                var endOfMonth = reportMonth.AddMonths(1);
                var firstMonth = new DateTime(reportMonth.Year, 1, 1);
                var prevMonth = reportMonth.AddMonths(-1);

                // ======================================================
                // Sheet 1 model - details report

                // Find last remind created in the previous month before `reportMonth`
                var query = db.Debts.Where(d => d.ImportMonth == prevMonth && d.MonthsOfDebt >= AppConstants.AlarmDebtMonths)
                    .Select(d => new {
                        Debt = d,
                        d.Company,
                        DebtReminds = d.Company.DebtReminds.Where(r => 
                            r.PaperType.PaperGroup == Models.PaperGroup.DebtReminding && 
                            r.PaperDate >= firstMonth && r.PaperDate < endOfMonth)
                    });

                // Kttn
                if (sendToKttn)
                {
                    query = query.Where(d => !d.DebtReminds.Any(r => r.SendToKttn));
                }

                _logger.Info($"{nameof(GenerateDebtRemindReport)} (Đôn đốc nợ / Bàn giao Kĩ thuật thu nợ) - sendToKttn: {sendToKttn}, templatePath: {templatePath}, outputFile: {outputFile}, " + 
                    $"reportMonth: {reportMonth}, reportType: {reportType}, \nquery: {query}");

                var debts = query.ToList();

                int rowNo = 1;
                var reportModel = new RemindDebtReportModel()
                {
                    ReportTitle = reportType == DebtRemindReportType.DebtRemindStatus ?
                        $"BÁO CÁO TÌNH HÌNH ĐÔN ĐỐC NỢ - Tháng {reportMonth.ToString(AppConstants.MonthFormat)}" :
                        $"BIÊN BẢN BÀN GIAO HỒ SƠ CÔNG NỢ - Tháng {reportMonth.ToString(AppConstants.MonthFormat)}",

                    DataRows = debts.Select(d =>
                    {
                        var debtReminds = string.Join(", ", d.DebtReminds.OrderBy(d2 => d2.PaperDate)
                            .Select(d2 => d2.PaperType.Name));

                        return new RemindDebtReportRowModel
                        {
                            SerialNo = rowNo++,
                            CompanyCode = d.Debt.CompanyCode,
                            CompanyName = d.Debt.CompanyName,
                            CompanyAddress = d.Debt.Address,
                            Officer = d.Company.Officer?.Name,
                            DebtMonths = (int)d.Debt.MonthsOfDebt,
                            LastMonthDebt = (int)d.Debt._tien_dk,
                            ThisMonthPaid = (int)d.Debt._tien_unc,
                            ThisMonthCollect = (int)Math.Min(d.Debt._tien_dk, d.Debt._tien_unc),
                            DebtReminds = debtReminds,
                            //HasD04H = d.DebtReminds.Any(r => r.PaperTypeId == 3),
                            //HasNotification1 = d.DebtReminds.Any(r => r.PaperTypeId == 4),
                            //HasNotification2 = d.DebtReminds.Any(r => r.PaperTypeId == 5),
                            SentToKttn = d.DebtReminds.Any(r => r.SendToKttn),
                        };
                    }).ToList(),
                };


                // ======================================================
                // Sheet 2 model - summary report
                
                // Find last remind created in the previous month of `reportMonth`
                var query2 = db.Debts.Where(d => d.ImportMonth == prevMonth)
                    .GroupBy(d => d.Officers)
                    .OrderBy(g => g.Key)
                    .Select(officerGroup => new
                    {
                        officerGroup.Key,
                        Group1 = officerGroup
                            .Where(g => g.MonthsOfDebt >= 1 || g.MonthsOfDebt <= 2),
                            //.Aggregate(new GroupInfo(), accuFunc),
                        Group2 = officerGroup
                            .Where(g => g.MonthsOfDebt >= 3 || g.MonthsOfDebt <= 5),
                            //.Aggregate(new GroupInfo(), accuFunc),
                        Group3 = officerGroup
                            .Where(g => g.MonthsOfDebt >= 6),
                            //.Aggregate(new GroupInfo(), accuFunc),
                    }).ToList();
                
                var reportModelSum = new RemindDebtReportSummaryModel()
                {
                    DataRows = query2.Select(r => new RemindDebtReportSummaryRowModel {
                        Officer = r.Key,
                        Count1 = r.Group1.Count(),
                        Debt1 = r.Group1.Sum(d => d._tien_dk),
                        Paid1 = r.Group1.Sum(d => d._tien_unc),
                        Collect1 = r.Group2.Sum(d => Math.Min(d._tien_dk, d._tien_unc)),

                        Count2 = r.Group2.Count(),
                        Debt2 = r.Group2.Sum(d => d._tien_dk),
                        Paid2 = r.Group2.Sum(d => d._tien_unc),
                        Collect2 = r.Group2.Sum(d => Math.Min(d._tien_dk, d._tien_unc)),

                        Count3 = r.Group3.Count(),
                        Debt3 = r.Group3.Sum(d => d._tien_dk),
                        Paid3 = r.Group3.Sum(d => d._tien_unc),
                        Collect3 = r.Group2.Sum(d => Math.Min(d._tien_dk, d._tien_unc)),
                    }).ToList()
                };

                // ======================================================
                // Convert to dataset
                var dataTable = new DataTable();
                {
                    // name-column mapping
                    var propInfos = typeof(RemindDebtReportRowModel).GetProperties();

                    var nameColMappings = new Dictionary<string, DataColumn>();
                    var rptFieldNameFieldInfoMappings = new Dictionary<string, PropertyInfo>();

                    var reportNameFieldMappings = GetDebtRemindReportFieldNames().ToList();
                    reportNameFieldMappings.ForEach(kv =>
                    {
                        var col = new DataColumn(kv.Key);
                        dataTable.Columns.Add(col);
                        nameColMappings.Add(kv.Key, col);
                        rptFieldNameFieldInfoMappings.Add(kv.Key, propInfos.First(p => p.Name == kv.Value));
                    });

                    // loop on rows
                    reportModel.DataRows.ForEach(row =>
                    {
                        var dataRow = dataTable.NewRow();
                    // loop on fields
                    reportNameFieldMappings.ForEach(kv =>
                        {
                            dataRow.SetField(nameColMappings[kv.Key], SDRxUtils.GetPropertyValue(row, rptFieldNameFieldInfoMappings[kv.Key]));
                        });
                        dataTable.Rows.Add(dataRow);
                    });
                }

                // ==================================
                // Convert to dataset
                var dataTableSum = new DataTable();
                {
                    // name-column mapping
                    var propInfosSum = typeof(RemindDebtReportSummaryRowModel).GetProperties();

                    var nameColMappingsSum = new Dictionary<string, DataColumn>();
                    var rptFieldNameFieldInfoMappingsSum = new Dictionary<string, PropertyInfo>();

                    var reportNameFieldMappingsSum = GetDebtRemindSummaryReportFieldNames().ToList();
                    reportNameFieldMappingsSum.ForEach(kv =>
                    {
                        var col = new DataColumn(kv.Key);
                        dataTableSum.Columns.Add(col);
                        nameColMappingsSum.Add(kv.Key, col);
                        rptFieldNameFieldInfoMappingsSum.Add(kv.Key, propInfosSum.First(p => p.Name == kv.Value));
                    });

                    // loop on rows
                    reportModelSum.DataRows.ForEach(row =>
                    {
                        var dataRow = dataTableSum.NewRow();
                    // loop on fields
                    reportNameFieldMappingsSum.ToList().ForEach(kv =>
                        {
                            dataRow.SetField(nameColMappingsSum[kv.Key], SDRxUtils.GetPropertyValue(row, rptFieldNameFieldInfoMappingsSum[kv.Key]));
                        });
                        dataTableSum.Rows.Add(dataRow);
                    });
                }
                // ======================================================
                // Bind to report

                var dataProps = xlsTemplate.CreateDataBindingProperties();

                // Bind header
                var fieldNames = new[] { AttributeUtils.GetAttribute<RemindDebtReportModel, ReportFieldName>
                    (nameof(RemindDebtReportModel.ReportTitle)).Value };
                var fieldValues = new[] { reportModel.ReportTitle };
                var dataSetName = AttributeUtils.GetAttribute<RemindDebtReportModel, ReportDataSetName>
                    (nameof(RemindDebtReportModel.ReportTitle)).Name;

                xlsTemplate.BindRowData(fieldValues, fieldNames, dataSetName, dataProps);

                // Bind details dataset
                var rowDatasetName = AttributeUtils.GetAttribute<RemindDebtReportModel, ReportDataSetName>
                    (nameof(RemindDebtReportModel.DataRows)).Name;
                xlsTemplate.BindData(dataTable, rowDatasetName, dataProps);

                // Bind summary dataset
                var summaryDatasetName = AttributeUtils.GetAttribute<RemindDebtReportSummaryModel, ReportDataSetName>
                    (nameof(RemindDebtReportSummaryModel.DataRows)).Name;
                xlsTemplate.BindData(dataTableSum, summaryDatasetName, dataProps);

                xlsTemplate.RemoveExtraDataMarkers = true;
                xlsTemplate.Process();
                xlsTemplate.Save(outputFile);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(GenerateDebtRemindReport)} error: {ex.Message} - {ex.StackTrace}");
                //MessageBox.Show($"Lỗi: {ex.Message}", "Lỗi sinh report");
                return false;
            }
        }

        /// <summary>
        /// BC Xử lý nợ của đơn vị
        /// </summary>
        /// <param name="templatePath"></param>
        /// <param name="outputFile"></param>
        /// <param name="companyId"></param>
        /// <param name="startMonth">First date of start month</param>
        /// <param name="endMonth">First date of end month - inclusive</param>
        /// <returns></returns>
        public static bool GenerateCompanyDebtDetailReport(string templatePath, string outputFile, int companyId, DateTime startMonth, DateTime endMonth)
        {
            // Month list is stored on HEADER set (%%=HEADER.MONTHx)
            // Common data fields are stored in DATA set
            //      Row:    %%=DATA.NO_EMPLOYEES01, %%=DATA.NO_EMPLOYEES02 ...
            //      Column: %%=DATA.NO_EMPLOYEES01, %%=DATA.PERIOD_START_DEBT01, %%=DATA.PERIOD_DEBT01 ...
            // DebtReminds are stored in DATA2 set
            //      Row:    %%=DATA2.REMIND_I_01, %%=DATA2.REMIND_I_02
            //      Column: %%=DATA2.REMIND_J_01, %%=DATA2.REMIND_J_02
            //      Where   I, J are debt remind type ID.

            //return false;

            if (endMonth < startMonth)
                throw new InvalidOperationException($"{nameof(endMonth)} cannot smaller than {nameof(startMonth)}");

            try
            {
                var xlsTemplate = new ExcelTemplate();
                xlsTemplate.Open(templatePath);

                var monthCount = 0;
                while (startMonth.AddMonths(monthCount) <= endMonth)
                    monthCount++;

                // one for last month
                monthCount++;

                //var reportNameFieldMappings = GetCompanyDebtProcessingReportFieldNames(monthCount);

                var company = db.Companies.First(c => c.Id == companyId);

                // Tìm tất cả dữ liệu nợ và đôn đốc nợ của khoảng thời gian cho trước.
                // Each month will be used for one month-column in report
                var query1 = db.Debts.Where(d => d.CompanyId == companyId
                    && d.ImportMonth >= startMonth && d.ImportMonth <= endMonth)
                    .Select(d => new CompanyDebtProcessingReportRowModel
                    {
                        DebtMonth = d.ImportMonth,
                        NoOfEmployees = (int)d.NoOfEmployees,
                        PeriodStartDebt = d._tien_dk,
                        PeriodDebt = d.tongBhCk + d.tongBst - d.tongBsg + d.OverdueInterests,
                        PeriodsDebtSI = d.tongBhCk + d.tongBst - d.tongBsg,
                        PeriodsDebtInterest = d.OverdueInterests,
                        //Reminds = d.a,
                        PaidAmount = d._tien_unc,
                        PeriodEndDebt = d._tien_ck,
                        PeriodEDebtSI = d._tien_ck - d._ck_lai,
                        PeriodEDebtInterest = d._ck_lai,
                        DebtMonths = (int)d.MonthsOfDebt,
                        Officer = d.Officers,
                    });

                var debtMonths = query1.ToList();

                _logger.Info($"{nameof(GenerateCompanyDebtDetailReport)} (BC Xử lý nợ của đơn vị) - templatePath: {templatePath}, outputFile: {outputFile}, \n" + 
                    $"companyId: {companyId}, startMonth: {startMonth}, endMonth: {endMonth}, query (Dữ liệu nợ): {query1}");

                var remindEnd = endMonth.AddMonths(1);
                var query2 = db.DebtReminds.Where(r => r.CompanyId == companyId 
                    && r.PaperType.PaperGroup == Models.PaperGroup.DebtReminding
                    && r.PaperDate >= startMonth && r.PaperDate < remindEnd).Include(r => r.PaperType);

                var reminds = query2.ToList();

                _logger.Info($"Query (Đôn đốc nợ): {query2}");

                var reportModel = new CompanyDebtProcessingReportModel(monthCount)
                {
                    ReportTitle = $"BÁO CÁO QUÁ TRÌNH XỬ LÝ NỢ CHI TIẾT THEO ĐƠN VỊ - {company.Name}",
                    ReportTitle2 = $"Từ tháng {startMonth.ToString(AppConstants.MonthFormat)} - Đến tháng {endMonth.ToString(AppConstants.MonthFormat)}",
                };

                // ==========================================
                // Bind header

                var monthRange = Enumerable.Range(0, monthCount).ToList();
                var dataProps = xlsTemplate.CreateDataBindingProperties();

                var modelType = typeof(CompanyDebtProcessingReportModel);
                var modelPropInfos = modelType.GetProperties();

                // Field names
                var fieldNames = new List<string> {
                    AttributeUtils.GetAttributeValue<ReportFieldName, string>(
                        modelPropInfos.First(m => m.Name == nameof(CompanyDebtProcessingReportModel.ReportTitle))),
                    AttributeUtils.GetAttributeValue<ReportFieldName, string>(
                        modelPropInfos.First(m => m.Name == nameof(CompanyDebtProcessingReportModel.ReportTitle2))),
                };

                fieldNames.AddRange(monthRange.Select(i => $"MONTH_{i:D2}"));

                // Field values
                var fieldValues = new List<string> { reportModel.ReportTitle, reportModel.ReportTitle2 };

                fieldValues.AddRange(monthRange.Select(i => startMonth.AddMonths(i).ToString(AppConstants.MonthFormat)));

                var dataSetName = AttributeUtils.GetAttribute<CompanyDebtProcessingReportModel, ReportDataSetName>().Name;

                xlsTemplate.BindRowData(fieldValues.ToArray(), fieldNames.ToArray(), dataSetName, dataProps);

                // ==========================================
                // Bind data rows - create dataset table columns (ONE ROW TABLE)

                // Convert to dataset
                var dataSet1 = new DataTable();

                var rowType = typeof(CompanyDebtProcessingReportRowModel);
                var propInfos = rowType.GetProperties();
                var nameColMappings = new Dictionary<string, DataColumn>();
                var reportFieldInfoMappings = new Dictionary<string, PropertyInfo>();

                // Loop report fields and months to create report cells
                foreach (var info in propInfos)
                {
                    // Each field of the report (except reminds) is just a SINGLE cell in dataTable
                    // There is ONLY ONE ROW !!

                    if (info.Name == nameof(CompanyDebtProcessingReportRowModel.Reminds) ||
                        info.Name == nameof(CompanyDebtProcessingReportRowModel.DebtMonth))
                        continue;

                    var fieldName = AttributeUtils.GetAttributeValue<ReportFieldName, string>(info);
                    reportFieldInfoMappings.Add(fieldName, info);

                    // Loop on months (report columns)
                    monthRange.ForEach(m =>
                    {
                        var col = new DataColumn($"{fieldName}_{m:D2}");
                        nameColMappings.Add(col.ColumnName, col);
                        dataSet1.Columns.Add(col);
                    });
                }

                // Loop to create remind columns
                var dataSet2 = new DataTable();

                var remindInfo = propInfos.First(p => p.Name == nameof(CompanyDebtProcessingReportRowModel.Reminds));
                monthRange.ForEach(m =>
                {
                    var fieldName = AttributeUtils.GetAttributeValue<ReportFieldName, string>(remindInfo);
                    var col = new DataColumn($"{fieldName}_{m:D2}");
                    dataSet2.Columns.Add(col);
                });

                // ==========================================
                // Bind data for non-remind fields (ONE ROW TABLE).
                var dataRow = dataSet1.NewRow();

                debtMonths.ForEach(debt =>
                {
                    var mIndex = monthRange.First(i => startMonth.AddMonths(i) == debt.DebtMonth);

                    // Loop on fields
                    reportFieldInfoMappings.ToList().ForEach(kv => {

                        // Get field name with month(column) index
                        var colName = $"{kv.Key}_{mIndex:D2}";

                        dataRow.SetField(nameColMappings[colName], SDRxUtils.GetPropertyValue(debt, kv.Value));
                    });
                });

                dataSet1.Rows.Add(dataRow);
                xlsTemplate.BindData(dataSet1, "DATA1", dataProps);

                // ==========================================
                // Bind data for remind fields

                var remindFieldName = RxUtils.GetReportFieldName<CompanyDebtProcessingReportRowModel>(
                    nameof(CompanyDebtProcessingReportRowModel.Reminds));

                // Only one row for all reminds
                // Each column is for one month, which combine all papers in that month
                var dataRow2 = dataSet2.NewRow();

                // Group reminds by month
                var gMonths = reminds.GroupBy(r => new DateTime(r.PaperDate.Year, r.PaperDate.Month, 1)).ToList();

                gMonths.ForEach(gMonth =>
                {
                    var mIndex = monthRange.First(i => startMonth.AddMonths(i) == gMonth.Key);

                    // colName name
                    var colName = $"{remindFieldName}_{mIndex:D2}";
                    var papers = string.Join(", ", gMonth.OrderBy(p => p.PaperDate).Select(r => r.PaperType.Name));

                    dataRow2.SetField(colName, papers);
                });

                dataSet2.Rows.Add(dataRow2);

                xlsTemplate.BindData(dataSet2, "DATA2", dataProps);

                xlsTemplate.RemoveExtraDataMarkers = true;
                xlsTemplate.Process();
                xlsTemplate.Save(outputFile);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(GenerateCompanyDebtDetailReport)} error: {ex.Message} - {ex.StackTrace}");
                //MessageBox.Show($"Lỗi: {ex.Message}", "Lỗi sinh report");
                return false;
            }
        }

        public static bool GenerateOldDebtProcessingReport__not_use(string templatePath, string outputFile, DateTime startMonth, DateTime endMonth)
        {
            // See how this func works from the comments for func GenerateCompanyDebtDetailReport above.

            //throw new InvalidOperationException("not implemented");

            if (endMonth < startMonth)
                throw new InvalidOperationException($"{nameof(endMonth)} cannot smaller than {nameof(startMonth)}");

            //*
            try
            {
                var xlsTemplate = new ExcelTemplate();
                xlsTemplate.Open(templatePath);

                var monthCount = 0;
                while (startMonth.AddMonths(monthCount) < endMonth)
                    monthCount++;

                // one for last month
                monthCount++;

                // Tìm tất cả dữ liệu nợ và đôn đốc nợ của khoảng thời gian cho trước.
                var debtMonths = db.Debts.Where(d => d.ImportMonth >= startMonth && d.ImportMonth <= endMonth
                        && d.MonthsOfDebt >= AppConstants.AlarmDebtMonths)
                    .Select(d => new OldDebtProcessingReportRowModel
                    {
                        DebtMonth = d.ImportMonth,

                        //StartNoOfDebtCompany = d.a,
                        //StartNoOfDebtCompany2M = d.a,
                        //StartNoOfDebtCompany6M = d.a,
                        //StartNoOfDebtCompany6pM = d.a,
                        //StartTotalDebt = d.a,
                        //StartTotalDebt2M = d.a,
                        //StartTotalDebt6M = d.a,
                        //StartTotalDebt6pM = d.a,
                        //MidTotalDebt = d.a,
                        //MidTotalDebt2M = d.a,
                        //MidTotalDebt6M = d.a,
                        //MidTotalDebt6pM = d.a,
                        //Reminds = d.a,
                        //PaidDebt = d.a,
                        //EndTotalDebt = d.a,
                        //EndTotalDebt2M = d.a,
                        //EndTotalDebt6M = d.a,
                        //EndTotalDebt6pM = d.a,
                    }).ToList();

                var remindEnd = endMonth.AddMonths(1);
                var reminds = db.DebtReminds.Where(r => r.PaperType.PaperGroup == Models.PaperGroup.DebtReminding
                    && r.PaperDate >= startMonth && r.PaperDate < remindEnd).Include(r => r.PaperType).ToList();

                var reportModel = new OldDebtProcessingReportModel(monthCount)
                {
                    //ReportTitle = $"BÁO CÁO QUÁ TRÌNH XỬ LÝ NỢ CHẬM ĐÓNG, NỢ NỢ KÉO DÀI, NỢ KHÓ THU",
                    ReportTitle2 = $"Từ tháng {startMonth.ToString(AppConstants.MonthFormat)} - Đến tháng {endMonth.ToString(AppConstants.MonthFormat)}",
                };

                // ==========================================
                // Bind header

                var monthRange = Enumerable.Range(0, monthCount).ToList();
                var dataProps = xlsTemplate.CreateDataBindingProperties();

                var modelType = typeof(OldDebtProcessingReportModel);
                var modelPropInfos = modelType.GetProperties();

                // Field names
                var fieldNames = new List<string> {
                    AttributeUtils.GetAttributeValue<ReportFieldName, string>(
                        modelPropInfos.First(m => m.Name == nameof(OldDebtProcessingReportModel.ReportTitle))),
                    AttributeUtils.GetAttributeValue<ReportFieldName, string>(
                        modelPropInfos.First(m => m.Name == nameof(OldDebtProcessingReportModel.ReportTitle2))),
                };

                fieldNames.AddRange(monthRange.Select(i => $"MONTH_{i:D2}"));

                // Field values
                var fieldValues = new List<string> { reportModel.ReportTitle, reportModel.ReportTitle2 };

                fieldValues.AddRange(monthRange.Select(i => startMonth.AddMonths(i).ToString(AppConstants.MonthFormat)));

                var dataSetName = AttributeUtils.GetAttribute<OldDebtProcessingReportModel, ReportDataSetName>().Name;

                xlsTemplate.BindRowData(fieldValues.ToArray(), fieldNames.ToArray(), dataSetName, dataProps);

                // ==========================================
                // Bind data rows - create dataset table columns (ONE ROW TABLE)

                // Convert to dataset
                var dataSet1 = new DataTable();

                var rowType = typeof(OldDebtProcessingReportRowModel);
                var propInfos = rowType.GetProperties();
                var nameColMappings = new Dictionary<string, DataColumn>();
                var reportFieldInfoMappings = new Dictionary<string, PropertyInfo>();

                // Loop report fields and months to create report cells
                foreach (var info in propInfos)
                {
                    // Each field of the report (except reminds) is just a SINGLE cell in dataTable
                    // There is ONLY ONE ROW !!

                    if (info.Name == nameof(OldDebtProcessingReportRowModel.Reminds) ||
                        info.Name == nameof(OldDebtProcessingReportRowModel.DebtMonth))
                        continue;

                    var fieldName = AttributeUtils.GetAttributeValue<ReportFieldName, string>(info);
                    reportFieldInfoMappings.Add(fieldName, info);

                    // Loop on months (report columns)
                    monthRange.ForEach(m =>
                    {
                        var col = new DataColumn($"{fieldName}_{m:D2}");
                        nameColMappings.Add(col.ColumnName, col);
                        dataSet1.Columns.Add(col);
                    });
                }

                // Loop to create remind columns
                var dataSet2 = new DataTable();

                var remindInfo = propInfos.First(p => p.Name == nameof(OldDebtProcessingReportRowModel.Reminds));
                monthRange.ForEach(m =>
                {
                    var fieldName = AttributeUtils.GetAttributeValue<ReportFieldName, string>(remindInfo);
                    var col = new DataColumn($"{fieldName}_{m:D2}");
                    dataSet2.Columns.Add(col);
                });

                // ==========================================
                // Bind data for non-remind fields (ONE ROW TABLE).
                var dataRow = dataSet1.NewRow();

                debtMonths.ForEach(debt =>
                {
                    var mIndex = monthRange.First(i => startMonth.AddMonths(i) == debt.DebtMonth);

                    // Loop on fields
                    reportFieldInfoMappings.ToList().ForEach(kv => {

                        // Get field name with month(column) index
                        var colName = $"{kv.Key}_{mIndex:D2}";

                        dataRow.SetField(nameColMappings[colName], SDRxUtils.GetPropertyValue(debt, kv.Value));
                    });
                });

                dataSet1.Rows.Add(dataRow);
                xlsTemplate.BindData(dataSet1, "DATA1", dataProps);

                // ==========================================
                // Bind data for remind fields

                var remindFieldName = RxUtils.GetReportFieldName<OldDebtProcessingReportRowModel>(
                    nameof(OldDebtProcessingReportRowModel.Reminds));

                // Only one row for all reminds
                // Each column is for one month, which combine all papers in that month
                var dataRow2 = dataSet2.NewRow();

                // Group reminds by month
                var gMonths = reminds.GroupBy(r => new DateTime(r.PaperDate.Year, r.PaperDate.Month, 1)).ToList();

                gMonths.ForEach(gMonth =>
                {
                    var mIndex = monthRange.First(i => startMonth.AddMonths(i) == gMonth.Key);

                    // colName name
                    var colName = $"{remindFieldName}_{mIndex:D2}";
                    var papers = string.Join(", ", gMonth.OrderBy(p => p.PaperDate).Select(r => r.PaperType.Name));

                    dataRow2.SetField(colName, papers);
                });

                dataSet2.Rows.Add(dataRow2);

                xlsTemplate.BindData(dataSet2, "DATA2", dataProps);

                xlsTemplate.RemoveExtraDataMarkers = true;
                xlsTemplate.Process();
                xlsTemplate.Save(outputFile);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error($"{nameof(GenerateOldDebtProcessingReport__not_use)} error: {ex.Message} - {ex.StackTrace}");
                //MessageBox.Show($"Lỗi: {ex.Message}", "Lỗi sinh report");
                return false;
            }

            //*/
        }
    }

    public enum DebtRemindReportType
    {
        Unknown,
        DebtRemindStatus,
        DebtRemindKttn,
    }
}
