﻿using DebtManagerWin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebtManagerWin.Utils
{
    public class TimeUtils
    {
        /// <summary>
        /// -count .. nearestMonth
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public static List<DateListItem> GenerateMonthsList(int count)
        {
            return GenerateMonthsList(count, DateTime.Now);
        }

        /// <summary>
        /// -count .. nearestMonth
        /// Work even if count is negative. lastMonnth is always included in resulted list.
        /// </summary>
        /// <param name="nearestMonth"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static List<DateListItem> GenerateMonthsList(int count, DateTime nearestMonth)
        {
            // Month list
            nearestMonth = new DateTime(nearestMonth.Year, nearestMonth.Month, 1);
            var firstMonth = nearestMonth.AddMonths(-count);

            // 1/-1
            var dir = count > 0 ? 1 : -1;
            count = Math.Abs(count);

            // List of count months
            var monthList = new List<DateListItem>(count);

            // i: 1/-1
            for (var i = 1; i <= count; i += 1)
            {
                var month = firstMonth.AddMonths(i * dir);

                monthList.Add(new DateListItem
                {
                    Value = month,
                    Format = AppConstants.MonthFormat
                });
            }

            monthList.Reverse();
            return monthList;
        }

        public static List<DateListItem> GenerateMonthsListForward(int count, DateTime firstMonth = default(DateTime))
        {
            if (firstMonth == default(DateTime))
                firstMonth = DateTime.Now;

            return GenerateMonthsList(-count, firstMonth);
        }
    }
}
