﻿using System;

namespace DebtManagerWin.Models
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class XlsFieldName : Attribute
    {
        public string Name { get; set; }

        public XlsFieldName(string name)
        {
            Name = name;
        }
    }

}
