﻿using SoftArtisans.OfficeWriter.WordWriter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            TestWriter();
        }

        static void TestWriter()
        {
            try
            {
                var wordTemplate = new WordTemplate();
                wordTemplate.Open(@"D:\MyProjects\debtmanager\Test\doc1.doc");

                wordTemplate.SetDataSource(new object[] { "val1", "val2", "val3" }, new[] { "Field1", "Field2", "Field3" });
                //wordTemplate.SetDataSource("val 1", "Field1");

                //wordTemplate.BookmarksToRemove()
                wordTemplate.Process();
                wordTemplate.Save(@"D:\MyProjects\debtmanager\Test\doc1-output.doc");
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error: {ex.Message}");
            }
        }
    }
}
